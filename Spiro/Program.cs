﻿using System;
using System.IO;
namespace Spiro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool pass = false;
#if DEBUG
#else
            try
            {
#endif
                if (args.Length >= 3)
                {
                    bool WriteToROM = false;

                if (args[0] == @"/PathToCyproFolder")
                {
                    if (!args[1].EndsWith(@"\"))
                    {
                        args[1] += @"\";
                    }

                    Main_manager m = new Main_manager(args[1]);

                    if ((args.Length >= 4 && args[3] == "/Verbose") || (args.Length == 5 && args[4] == "/Verbose"))
                    {
                        Global.Verbose = true;
                    }

                    if (args[2] == "/WriteScriptToROM" || args[2] == "/Write")
                        {
                            m.WriteScriptToROM();
                            pass = true;
                            WriteToROM = true;
                        }
                        else if (args[2] == "/DumpScript" || args[2] == "/Dump")
                        {
                            bool CreateAceFiles = false;
                            if ((args.Length >= 4 && args[3] == "/CreateAceFiles") || (args.Length == 5 && args[4] == "/CreateAceFiles"))
                            {
                                CreateAceFiles = true;
                            }

                            m.DumpScript(CreateAceFiles);
                            pass = true;
                            Console.WriteLine("Dumped script to XML.");

                            if (Settings.BlankOutTextDataAfterRead)
                            {
                                //WriteToROM = true;
                            }
                        }

                        if (pass)
                        {
                            if (WriteToROM)
                            {
                                File.Delete(Settings.ROMToWrite);
                                File.Copy(Settings.ROMToCopy, Settings.ROMToWrite);
                                File.WriteAllBytes(Settings.ROMToWrite, ROM.DATA);
                                Console.WriteLine("Wrote script to ROM successfully.");
                            }
                        }
                    }
                    Console.WriteLine("Finished successfully.");

                }
#if DEBUG
#else
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message.ToString());
                pass = true;
            }
            finally
            {
#endif
                if (!pass)
                {
                    Console.WriteLine("Error: unrecognized or incomplete command line.\n");
                    Console.WriteLine("USAGE:");
                    Console.WriteLine("    File.exe:");
                    Console.WriteLine("        /PathToCyproFolder $PATH");
                    Console.WriteLine("        [/WriteScriptToROM | /ReadScript]");
                    Console.WriteLine("        [/CreateAceFiles]");
                    Console.WriteLine("        [/Verbose]");

                    Console.WriteLine("    Options:");
                    Console.WriteLine("       /WriteScriptToROM     Write the script to the ROM file. File name is from your XML cypro file.");
                    Console.WriteLine("       /DumpScript           Dump script to XML file.");
                    Console.WriteLine("       /CreateAceFiles       Create Comment, Menu, and New XML files when dumping script.");
                    Console.WriteLine("       /Verbose              Verbose output.");
                }

#if DEBUG
#else
            }
#endif
        }
    }
}