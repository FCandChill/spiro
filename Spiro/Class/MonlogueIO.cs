﻿/* Class        : MonologueIO.cs
 * Author       : FCAndChill, clampsy
 * Description  : Deals with data conversion for monologue editor
 */

using System.Collections.Generic;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using static Enums;

internal class MonologueIO
{
    public readonly MyDictionary chara;

    public MonologueIO(bool LoadDict, bool ReadOriginal)
    {
        chara = new MyDictionary(LoadDict, ReadOriginal);
    }

    private void BlankOutData(int Pointer, int Size)
    {
        if (Settings.BlankOutTextDataAfterRead)
        {
            /*
             * Don't blank out the end of line character becuase data can overlap
             * and make the editor read longer than it should.
             */

            for (int i = Pointer; i < Pointer + Size; i++)
            {
                ROM.DATA[i] = Settings.BlankOutByte;
            }
        }
    }

    public List<byte> ReadByteStream_HarvestMoonMineralTown(ulong[] PC_Pointer, int IndexTable, int Entry, byte[][] Delimeter, int DictionaryInteractionType, ref List<byte> Script_withoutDelimester)
    {
        List<byte> ByteDigest = new List<byte>();
        foreach (byte[] b in Delimeter)
        {
            List<byte> l = ReadByteStream_with_pointer_and_delim(PC_Pointer, IndexTable, Entry, new byte[][] { b }, DictionaryInteractionType, ref Script_withoutDelimester);
            PC_Pointer[Entry] += (ulong)l.Count;
            ByteDigest = ByteDigest.Concat(l).ToList();
        }
        return ByteDigest;
    }

    public List<byte> ReadByteStream_with_pointer_in_size(ulong[] PC_Pointer, int[] Length, int Entry)
    {
        //Console.WriteLine(string.Format("{0}/{1}", (Entry + 1).ToString("0000"), PC_Pointer.Length));

        if (Length[Entry] < 0)
        {
            throw new Exception("Array index can't be negative");
        }

        byte[] byteData = new byte[Length[Entry]];

        ulong PC_Address = PC_Pointer[Entry];
        int v_Length = Length[Entry];

        Array.Copy(ROM.DATA, (int)PC_Address, byteData, 0, v_Length);

        BlankOutData((int)PC_Address, v_Length);

        return byteData.ToList();
    }

    private void RemoveDelimeter(List<byte> byteList, byte[][] Delimeter, out List<byte> ScriptBytes_NoDelimeter)
    {
        byte[] byteArray = byteList.ToArray();
        ScriptBytes_NoDelimeter = new List<byte>();
        int i = 0;

        for (; ; )
        {
            for (int entryindex = 0; entryindex < Delimeter.Length; entryindex++)
            {
                for (int j = i; j < i + Delimeter[entryindex].Length + 1; j++)
                {
                    if (j - i == Delimeter[entryindex].Length)
                    {
                        goto Getout;
                    }

                    if (!(j - i < Delimeter[entryindex].Length && byteArray[j] == Delimeter[entryindex][j - i]))
                    {
                        ScriptBytes_NoDelimeter.Add(byteArray[i]);
                        i++;
                        break;
                    }
                }
            }

            if (Delimeter.Length == 0)
            {
                ScriptBytes_NoDelimeter = byteList;
                break;
            }
        }

    Getout:;
    }

    public List<byte> ConvertScriptToHex_WithoutDictionary(string line, int EntryNo, out List<byte> ScriptBytes_NoDelimeter, byte[][] Delimeter)
    {
        List<byte> output = new List<byte>();

        for (int i = 0; i < line.Length; i++)
        {
            string Character = "";
            int length = 0;
            byte[] possibleKey = new byte[0];

            bool HexCharacter = false;
            do
            {
                if (++length > line.Length - i)
                {
                    throw new Exception($"Cannot insert script for entry #{EntryNo}. Couldn't insert character: \"{Character}\"");
                }
                else if (Character == Constants.BYTE1.ToString())
                {
                    HexCharacter = true;
                    break;
                }

                Character = line.Substring(i, length);
            }
            while (!chara.GetCHRByteValue(Character, out possibleKey));

            if (!HexCharacter)
            {
                foreach (byte b in possibleKey)
                {
                    output.Add(b);
                }

                i += Character.Length - 1;
            }
            else
            {
                //If a hex character. Get the hex value.
                string HexCharacter_As_String = "";
                length = 0;

                //Read the byte data inside the brackets. Includes brackets.
                do
                {
                    if (++length > line.Length - i)
                    {
                        throw new Exception();
                    }

                    HexCharacter_As_String = line.Substring(i, length);
                }
                while (!HexCharacter_As_String.Contains(Constants.BYTE2.ToString()));

                //Use Regex to remove the brackets 
                //or any other miscellaneous characters that aren't hexadecimal characters.
                Regex regex = new Regex(@"[^A-Fa-f0-9]");
                string result = regex.Replace(HexCharacter_As_String, string.Empty);
                int HexValue = MyMath.hexToDec(result);
                output.Add((byte)HexValue);
                i += HexCharacter_As_String.Length - 1;
            }
        }

        RemoveDelimeter(output, Delimeter, out ScriptBytes_NoDelimeter);

        return output;
    }

    struct Embed
    {
        public string[] BytePattern { private set; get; }
        public ulong Location { private set; get; }

        public Embed(string BytePattern, ulong Location)
        {
            this.BytePattern = BytePattern.Split(',');
            this.Location = Location;
        }
    }

    public List<byte> ConvertScriptToHex_WithDictionary(string line, int EntryNo)
    {
        SortedDictionary<int, Embed> Embed_End = new SortedDictionary<int, Embed>();
        SortedDictionary<int, Embed> EmbedRefList = new SortedDictionary<int, Embed>();

        ulong ii = 0;
        ulong ii_prev = 0;

        List<byte> output = new List<byte>();
        List<KeyValuePair<byte[], byte[]>> Byte_Dictionary;
        List<KeyValuePair<byte[], string>> String_Dictionary;
        int BiggestEntry;


        Regex regex1 = new Regex(Constants.REGEX_SPLIT_STRING);

        string[] a_SubChunk = regex1.Split(line).Where(s => s != string.Empty).ToArray();


        for (int i = 0; i < a_SubChunk.Length;)
        {
            string Chunk = a_SubChunk[i];

            if (chara.Dict_Sort_By_Character_With_String_Val.ContainsKey(Chunk))
            {
                Byte_Dictionary = chara.Dict_Sort_By_Character_With_Byte_Val[Chunk];
                String_Dictionary = chara.Dict_Sort_By_Character_With_String_Val[Chunk];
                BiggestEntry = chara.Dict_Sort_By_Character_With_String_Val_MaxLength[Chunk];

                Byte_Dictionary.Sort((x, y) => y.Value.Length.CompareTo(x.Value.Length));
                String_Dictionary.Sort((x, y) => y.Value.Length.CompareTo(x.Value.Length));

                string[] ss;

                if (BiggestEntry + i <= a_SubChunk.Length)
                {
                    ss = new string[BiggestEntry];
                    Array.Copy(a_SubChunk, i, ss, 0, BiggestEntry);
                }
                else
                {
                    ss = new string[a_SubChunk.Length - i];
                    Array.Copy(a_SubChunk, i, ss, 0, ss.Length);
                }

                string FindMe = "";
                foreach (string s in ss)
                {
                    FindMe += s;
                }

                foreach (KeyValuePair<byte[], string> kvp in String_Dictionary)
                {
                    string Dictionary_Entry = kvp.Value;
                    if (FindMe.StartsWith(Dictionary_Entry))
                    {
                        foreach (byte b in kvp.Key)
                        {
                            output.Add(b);
                            ii++;
                        }

                        i += (regex1.Split(Dictionary_Entry).Where(s => s != string.Empty).ToArray()).Length;
                        goto Getout; //we insrted the byte data. Now break out of the nested loop.
                    }
                }

                throw new Exception($"Cannot insert script for entry #{EntryNo}. Couldn't insert character: \"{FindMe}\"");

            Getout:;

            }
            else if (Chunk.Contains("{"))
            {
                string result = (new Regex(@"[^A-Fa-f0-9]")).Replace(Chunk, string.Empty);

                if (result == string.Empty)
                {
                    throw new Exception($"Cannot insert script for entry #{EntryNo}. Hex character malformed.");
                }

                int HexValue = MyMath.hexToDec(result);
                output.Add((byte)HexValue);
                i++;
            }
            else if (Chunk.Contains("<"))
            {
                Chunk = Chunk.Replace("<", "").Replace(">", "");

                bool Reset = false;

                if (Chunk.Contains("EmbeddedRedirectReset "))
                {
                    Reset = true;
                    Chunk = Chunk.Replace("EmbeddedRedirectReset ", "");
                }
                else
                {
                    Chunk = Chunk.Replace("EmbeddedRedirect ", "");
                }
                string[] spl = Chunk.Split(' ');


                if (spl.Length != 2)
                {
                    ii++;

                    ulong Location = 0;

                    if (!Reset)
                    {
                        Location = ii;
                    }
                    else
                    {
                        Location = ii - ii_prev;
                        ii_prev = ii;
                    }
                    Embed_End.Add(int.Parse(spl[0]), new Embed(spl[1], Location));


                }
                else
                {
                    EmbedRefList.Add(int.Parse(spl[0]), new Embed(spl[1], ii));

                    for (int j = 0; j < spl[1].Replace(",", "").Replace("(", "").Replace(")", "").Length / 2; j++)
                    {
                        //can be any value, doesn't matter
                        //Will be overwritten anyway.
                        output.Add(0xc5);
                    }
                }

                i++;
            }
            else
            {
                throw new Exception($"Entry no: {EntryNo}.Bad chunk {Chunk}. Likely not in dictionary.");
            }

        }

        if (Embed_End.Count != EmbedRefList.Count)
        {
            throw new Exception("Embed pointer and reference mismatch.");
        }

        byte[] a_output = output.ToArray();

        PointerManager ptr = new PointerManager();
        for (int i = 0; i < Embed_End.Count; i++)
        {
            byte[] ByteArray = ptr.CreateAddress_CustomFormat(Embed_End[i].Location, Embed_End[i].BytePattern);


            Array.Copy(ByteArray, 0, a_output, (int)EmbedRefList[i].Location, ByteArray.Length);
        }

        return a_output.ToList();
    }

    public List<byte> ReadByteStream_with_pointer_and_delim(ulong[] PC_Pointer, int IndexTable, int Entry, byte[][] Delimeter, int DictionaryInteractionType, ref List<byte> Script_withoutDelimester)
    {
        List<byte> byteList = new List<byte>();

        int pos = (int)PC_Pointer[Entry];

        string s = "";

        if (pos >= 0)
        {
            int i = pos;

            byte LookAhead(int index, out bool IsOutOfBounds)
            {
                IsOutOfBounds = false;
                if (index < ROM.DATA.Length)
                {
                    return ROM.DATA[index];
                }
                else
                {
                    IsOutOfBounds = true;
                    return 0;
                }
            }

            byte b1, b2, b3, b4;
            byte[] Dict_1, Dict_2, Dict_3, Dict_4;
            byte[] lookMeUp = new byte[0];
            bool LookupDict_1, LookupDict_2, LookupDict_3, LookupDict_4;
            while (true)
            {
                if (!(i < ROM.DATA.Length))
                {
                    throw new Exception("Attempted to readoutside the ROM.");
                }

                if (byteList.Count >= 10000000)
                {
                    throw new Exception($"The byte count is {byteList.Count}. It's getting rather long, so you're probably reading invalid data at this point.");
                }

                b1 = LookAhead(i + 0, out bool OutOfBounds1);
                b2 = LookAhead(i + 1, out bool OutOfBounds2);
                b3 = LookAhead(i + 2, out bool OutOfBounds3);
                b4 = LookAhead(i + 3, out bool OutOfBounds4);

                bool IsEnd = false;

                for (int entryindex = 0; entryindex < Delimeter.Length; entryindex++)
                {
                    for (int j = i; j < i + Delimeter[entryindex].Length + 1; j++)
                    {
                        if (j - i == Delimeter[entryindex].Length)
                        {
                            IsEnd = true;
                            Script_withoutDelimester = byteList;
                            byteList = byteList.Concat(Delimeter[entryindex]).ToList();
                            goto Getout;
                        }

                        if (!(j - i < Delimeter[entryindex].Length && ROM.Read(j) == Delimeter[entryindex][j - i]))
                        {
                            IsEnd = false;
                            break;
                        }
                    }
                }

            Getout:;

                if (IsEnd)
                {
                    break;
                }

                Dict_1 = new byte[] { b1 };
                Dict_2 = new byte[] { b1, b2 };
                Dict_3 = new byte[] { b1, b2, b3 };
                Dict_4 = new byte[] { b1, b2, b3, b4 };

                switch (DictionaryInteractionType)
                {
                    case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToCHRDirect_ExceptForControlCode:
                        LookupDict_1 = chara.DictionaryContainsKey(Dict_1);
                        if (!LookupDict_1)
                        {
                            LookupDict_1 = chara.Dict_Controlcode.ContainsKey(Dict_1);
                        }

                        LookupDict_2 = chara.DictionaryContainsKey(Dict_2);
                        if (!LookupDict_2)
                        {
                            LookupDict_2 = chara.Dict_Controlcode.ContainsKey(Dict_2);
                        }
                        LookupDict_3 = chara.DictionaryContainsKey(Dict_3);
                        if (!LookupDict_3)
                        {
                            LookupDict_3 = chara.Dict_Controlcode.ContainsKey(Dict_3);
                        }

                        LookupDict_4 = chara.DictionaryContainsKey(Dict_4);
                        if (!LookupDict_4)
                        {
                            LookupDict_4 = chara.Dict_Controlcode.ContainsKey(Dict_4);
                        }
                        break;
                    case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToDictionaryToCHR:
                        LookupDict_1 = chara.DictionaryContainsKey(Dict_1);
                        LookupDict_2 = chara.DictionaryContainsKey(Dict_2);
                        LookupDict_3 = chara.DictionaryContainsKey(Dict_3);
                        LookupDict_4 = chara.DictionaryContainsKey(Dict_4);
                        break;
                    default:
                        throw new Exception();
                }


                bool DictionaryEntryFound = true;
                if (LookupDict_4 && !OutOfBounds4)
                {
                    byteList.Add(b1);
                    byteList.Add(b2);
                    byteList.Add(b3);
                    byteList.Add(b4);

                    i += 4;
                    lookMeUp = Dict_4;
                }
                else if (LookupDict_3 && !OutOfBounds3)
                {
                    byteList.Add(b1);
                    byteList.Add(b2);
                    byteList.Add(b3);

                    i += 3;
                    lookMeUp = Dict_3;
                }
                else if (LookupDict_2 && !OutOfBounds2)
                {
                    byteList.Add(b1);
                    byteList.Add(b2);
                    i += 2;
                    lookMeUp = Dict_2;

                }
                else if (LookupDict_1 && !OutOfBounds1)
                {
                    byteList.Add(b1);

                    i += 1;
                    lookMeUp = Dict_1;
                }
                else
                {
                    byteList.Add(b1);
                    DictionaryEntryFound = false;
                    i += 1;
                    lookMeUp = new byte[0];

                    s += Constants.BYTE1 + MyMath.decToHex(b1) + Constants.BYTE2;
                }

                if (DictionaryEntryFound)
                {
                    string Character = "";
                    int ArgumentLength = 0;
                    switch (DictionaryInteractionType)
                    {
                        case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToCHRDirect_ExceptForControlCode:
                            Character = chara.GetDictionaryKeyAsString_DictionaryReferencesCHRExceptForControlCodes(lookMeUp, out ArgumentLength);
                            break;
                        case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToDictionaryToCHR:
                            Character = chara.GetDictionaryKeyAsString_DictionaryReferencesDictionary(lookMeUp, out ArgumentLength);
                            break;
                        default:
                            throw new Exception();
                    }

                    //For Earthbound's "(Multiple-Address Reference Pointer MULTIPLIER)"
                    if (Character.Contains("MULTIPLIER"))
                    {
                        ArgumentLength *= ROM.DATA[i];
                    }

                    s += Character;

                    while (ArgumentLength-- > Constants.CODE_ARGUMENT_LENGTH_NULL)
                    {
                        s += Constants.BYTE1 + MyMath.decToHex(ROM.DATA[i]) + Constants.BYTE2;
                        byteList.Add(ROM.DATA[i++]);
                    }
                }
            }
            BlankOutData(pos, i - pos);

        }
        else
        {
            if (!Settings.Pointer_Read_info[IndexTable].IgnoreNegativeAndZeroPointers)
            {
                throw new Exception("Pointer value not positive with no override.");
            }
        }
        return byteList;
    }

    private string toByteCharacter(byte b) => Constants.BYTE1 + MyMath.decToHex(b) + Constants.BYTE2;

    public string BytesToString_MetalSladerNameCards(List<byte> Compressed)
    {
        string script = "";

        if (Compressed.Count > 1)
        {
            const byte METAL_SLADER_CODE = 0x00;

            if (Compressed[0] == METAL_SLADER_CODE)
            {
                byte[] b_Compressed = Compressed.ToArray();

                /*
                 * If 00 00 
                 * Then it's just bytes of code.
                 */
                if (Compressed[1] == METAL_SLADER_CODE)
                {
                    foreach (byte b in Compressed)
                    {
                        script += toByteCharacter(b);
                    }
                }
                /*
                 * Otherwise, it's a namecard.
                 */
                else
                {
                    script += toByteCharacter(b_Compressed[0]);
                    script += toByteCharacter(b_Compressed[1]);

                    int NameCardSize = b_Compressed[1];
                    byte[] namecard = new byte[NameCardSize];

                    Array.Copy(b_Compressed, 2, namecard, 0, NameCardSize);

                    script += BytesToString_NoDictionary(namecard.ToList(), false);

                    for (int i = NameCardSize + 2; i < b_Compressed.Length; i++)
                    {
                        script += toByteCharacter(b_Compressed[i]);
                    }
                }
            }
            else script = BytesToString_NoDictionary(Compressed, false);
        }
        else
        {
            script = BytesToString_NoDictionary(Compressed, false);
        }
        return script;
    }

    public string BytesToString_Dictionary_Table(List<byte> Compressed, int DictionaryInteractionType)
    {
        string str_line = "";
        byte[] Byte_data = Compressed.ToArray();

        int i = 0;

        byte LookAhead(int index, out bool IsOutOfBounds)
        {
            IsOutOfBounds = false;
            if (index < Byte_data.Length)
            {
                return Byte_data[index];
            }
            else
            {
                IsOutOfBounds = true;
                return 0;
            }
        }

        byte b1, b2, b3, b4;
        byte[] Dict_1, Dict_2, Dict_3, Dict_4;
        byte[] lookMeUp = new byte[] { };
        bool LookupDict_1, LookupDict_2, LookupDict_3, LookupDict_4;
        while (i < Byte_data.Length)
        {
            b1 = LookAhead(i + 0, out bool OutOfBounds1);
            b2 = LookAhead(i + 1, out bool OutOfBounds2);
            b3 = LookAhead(i + 2, out bool OutOfBounds3);
            b4 = LookAhead(i + 3, out bool OutOfBounds4);

            Dict_1 = new byte[] { b1 };
            Dict_2 = new byte[] { b1, b2 };
            Dict_3 = new byte[] { b1, b2, b3 };
            Dict_4 = new byte[] { b1, b2, b3, b4 };

            switch (DictionaryInteractionType)
            {
                case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToCHRDirect_ExceptForControlCode:
                    LookupDict_1 = chara.DictionaryContainsKey(Dict_1);
                    if (!LookupDict_1)
                    {
                        LookupDict_1 = chara.Dict_Controlcode.ContainsKey(Dict_1);
                    }

                    LookupDict_2 = chara.DictionaryContainsKey(Dict_2);
                    if (!LookupDict_2)
                    {
                        LookupDict_2 = chara.Dict_Controlcode.ContainsKey(Dict_2);
                    }
                    LookupDict_3 = chara.DictionaryContainsKey(Dict_3);
                    if (!LookupDict_3)
                    {
                        LookupDict_3 = chara.Dict_Controlcode.ContainsKey(Dict_3);
                    }

                    LookupDict_4 = chara.DictionaryContainsKey(Dict_4);
                    if (!LookupDict_4)
                    {
                        LookupDict_4 = chara.Dict_Controlcode.ContainsKey(Dict_4);
                    }
                    break;
                case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToDictionaryToCHR:
                    LookupDict_1 = chara.DictionaryContainsKey(Dict_1);
                    LookupDict_2 = chara.DictionaryContainsKey(Dict_2);
                    LookupDict_3 = chara.DictionaryContainsKey(Dict_3);
                    LookupDict_4 = chara.DictionaryContainsKey(Dict_4);
                    break;
                default:
                    throw new Exception();
            }

            bool DictionaryEntryFound = true;

            if (LookupDict_4 && !OutOfBounds4)
            {
                i += 4;
                lookMeUp = Dict_4;
            }
            else if (LookupDict_3 && !OutOfBounds3)
            {
                i += 3;
                lookMeUp = Dict_3;
            }
            else if (LookupDict_2 && !OutOfBounds2)
            {
                i += 2;
                lookMeUp = Dict_2;
            }
            else if (LookupDict_1 && !OutOfBounds1)
            {
                i += 1;
                lookMeUp = Dict_1;
            }
            else
            {
                i += 1;
                lookMeUp = new byte[] { };
                DictionaryEntryFound = false;
            }

            if (DictionaryEntryFound)
            {
                string FoundDictinoaryValue;
                int ArgumentLength;
                switch (DictionaryInteractionType)
                {
                    case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToCHRDirect_ExceptForControlCode:
                        FoundDictinoaryValue = chara.GetDictionaryKeyAsString_DictionaryReferencesCHRExceptForControlCodes(lookMeUp, out ArgumentLength);
                        break;
                    case (int)DialogueDictionaryRender_DictionaryInteractionType.DictionaryToDictionaryToCHR:
                        FoundDictinoaryValue = chara.GetDictionaryKeyAsString_DictionaryReferencesDictionary(lookMeUp, out ArgumentLength);
                        break;
                    default:
                        throw new Exception();
                }

                str_line += FoundDictinoaryValue;

                //For Earthbound's "(Multiple-Address Reference Pointer MULTIPLIER)"
                if (FoundDictinoaryValue.Contains("MULTIPLIER"))
                {
                    ArgumentLength *= Byte_data[i];
                }


                while (ArgumentLength-- > Constants.CODE_ARGUMENT_LENGTH_NULL)
                {
                    if (Byte_data.Length > i)
                    {
                        str_line += toByteCharacter(Byte_data[i++]);
                    }
                    else
                    {
                        throw new Exception("String ends to early. Control code expected an argument.");
                    }
                }
            }
            else
            {
                str_line += Constants.BYTE1 + MyMath.decToHex(b1) + Constants.BYTE2;
            }
        }
        return str_line;
    }

    public string BytesToString_NoDictionary(List<byte> ByteStream, bool BigEndian)
    {
        byte[] a_byte = ByteStream.ToArray();
        string DialogueLine = "";

        byte LookAhead(int index, out bool IsOutOfBounds)
        {
            IsOutOfBounds = false;
            if (index < a_byte.Length)
            {
                return a_byte[index];
            }
            else
            {
                IsOutOfBounds = true;
                return 0;
            }
        }

        byte b1, b2, b3, b4;
        byte[] CHR_1, CHR_2, CHR_3, CHR_4;
        for (int i = 0; i < a_byte.Length;)
        {
            b1 = LookAhead(i + 0, out bool _);
            b2 = LookAhead(i + 1, out bool OutOfBounds2);
            b3 = LookAhead(i + 2, out bool OutOfBounds3);
            b4 = LookAhead(i + 3, out bool OutOfBounds4);

            if (BigEndian)
            {
                CHR_1 = new byte[] { b1 };
                CHR_2 = new byte[] { b2, b1 };
                CHR_3 = new byte[] { b3, b2, b1 };
                CHR_4 = new byte[] { b4, b3, b2, b1 };
            }
            else
            {
                CHR_1 = new byte[] { b1 };
                CHR_2 = new byte[] { b1, b2 };
                CHR_3 = new byte[] { b1, b2, b3 };
                CHR_4 = new byte[] { b1, b2, b3, b4 };
            }

            if (chara.IsCHRValid(CHR_4) && !OutOfBounds4)
            {
                i += 4;
                DialogueLine += chara.GetCHRValue(CHR_4);
            }
            else if (chara.IsCHRValid(CHR_3) && !OutOfBounds3)
            {
                i += 3;
                DialogueLine += chara.GetCHRValue(CHR_3);
            }
            else if (chara.IsCHRValid(CHR_2) && !OutOfBounds2)
            {
                i += 2;
                DialogueLine += chara.GetCHRValue(CHR_2);
            }
            else
            {
                i += 1;
                DialogueLine += chara.GetCHRValue(CHR_1);
            }
        }
        return DialogueLine;
    }
}