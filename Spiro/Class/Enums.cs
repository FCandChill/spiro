﻿public class Enums
{
    public enum PointerType
    {
        None = 0,
        LittleEndian = 1,
        Three_Byte_Metal_Slader_DX_Primary = 2,
        Three_Byte_Metal_Slader_DX_Primary_NoLength = 3,
        Three_Byte_MetalSlader_NES_Primary = 4,
        Three_Byte_MetalSlader_NES_Primary_NoLength = 5,
        Three_Byte_MetalSlader_NES_trans_Primary_NoLength = 6,
        BigEndian = 7,
        Arle_Adventure_GBC_7_Byte_Pointer = 8,
        NoDelimeterNoPointer_Fixed = 9,
        NoPointer_Delimeter = 10,
        Custom = 11
    }

    public enum DialogueReadType
    {
        HasDelimeter = 0,
        HasLengthVar = 1
    }

    public enum AddressConversion
    {
        None = 0,
        LOROM = 1,
        HiROM = 2
    }

    public enum AutoLineBreak
    {
        None = 0,
        MetalSladerGlory = 1,
        TokimekiMiho = 2,
        TokimekiMihoMessage = 3,
    }

    public enum e_DataType
    {
        NoDataTpye,
        IntAddress,
        ByteArray
    }

    public enum RenderType
    {
        NoDictionary_BigEndian = 0,
        Dictionary = 1,
        NoDictionary_MetalSladerNameCard = 2,
        NoDictionary_LittleEndian = 4,
        Dictionary_HarvestMoonMineralTown = 5,
    }

    public enum DialogueDictionaryRender_DictionaryInteractionType
    {
        NULL = 0,
        DictionaryToCHRDirect_ExceptForControlCode = 1,
        DictionaryToDictionaryToCHR = 2
    }
}