﻿/* 
 * Class        :   CharacterTable.cs
 * Author       :   FCandChill
 * Consultant   :   Kaijitani-Eizan
 * Description  :   
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Stores the width of each character. For auto-line breaking.
/// 
/// A lot of logic is borrowed from MonologueIO.
/// </summary>
class WidthCalc
{
    private string Filename { get; set; }

    /// <summary>
    /// Key     = CHR hex value
    /// Value   = length
    /// </summary>
    private SortedDictionary<byte[], int> CHR_Lengthlookup { get; set; }
    private SortedDictionary<byte[], int> ControlCode_Lengthlookup { get; set; }

    private Queue<byte[]> DictionaryKeys { get; set; }

    /*
     * It was pointed out that it's weird that the constructor is empty.
     * The reason why it's setup this way is so you can swap CHR files
     * easily.
     */
    public WidthCalc() { }

    public void LoadCHR(string FilenamePart)
    {
        Filename = string.Format(Constants.Path_To_x_length, FilenamePart);
        CHR_Lengthlookup = new SortedDictionary<byte[], int>(new ByteComparer());

        using (FileStream inputOpenedFile = File.Open(Path.Combine(Global.MainFolder, Filename), FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8))
            {
                foreach (string fileLine in sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    byte[] HexValue = GetHexValueFromFileLine(fileLine);
                    int Length = ParseLengthFileLine(fileLine);
                    CHR_Lengthlookup.Add(HexValue, Length);
                }
            }
        }
    }

    public void LoadDictionaryTemplate(string FilenamePart)
    {
        Filename = string.Format(Constants.Path_To_dict_template, FilenamePart);
        DictionaryKeys = new Queue<byte[]>();
        ControlCode_Lengthlookup = new SortedDictionary<byte[], int>(new ByteComparer());

        using (FileStream inputOpenedFile = File.Open(Path.Combine(Global.MainFolder, Filename), FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8))
            {
                foreach (string fileLine in sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!fileLine.StartsWith("//"))
                    {
                        byte[] HexValue = GetHexValueFromFileLine(fileLine);
                        DictionaryKeys.Enqueue(HexValue);
                    }
                }
            }
        }
    }


    public string TokimekiMiho_Trans_AddLineBreaks(int EntryNumber, string DialogueEntry, int PixelsPerLine, MyDictionary chara, bool AutoEdgeLinebreak)
    {
        //Split the dialogue so my control codes are in one index while the dialogue is 
        string[] Spiro_controlCode = DialogueEntry.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
        DialogueEntry = Regex.Replace(DialogueEntry, @"\\.*?/", "");

        /*
         * Parse my own control codes.
         * LineWidth = Skips the linebreaking code entirely
         * LineBreakSkip = let's you set the width of the textbox
         */

        bool OverrideLineSkip = false;
        bool LineAfterwards = false;
        string NewScriptEntry = "";

        if (Spiro_controlCode.Length > 1)
        {
            foreach (string s in Spiro_controlCode)
            {
                if (s.StartsWith("LineWidth"))
                {
                    PixelsPerLine = int.Parse(s.Split('=')[1]);
                }
                else if (s.Equals("LineBreakSkip"))
                {
                    NewScriptEntry = DialogueEntry;
                    OverrideLineSkip = true;
                    break;
                }
                else if (s.Equals("LineAfterwards"))
                {
                    LineAfterwards = true;
                    break;
                }
            }
        }

        if (!OverrideLineSkip)
        {
            const string
                CODE_NAMECARD_COLON = ":";

            /*
             * Split the dialogue string into chunks that we can work with.
             * Space get their own entries.
             * 
             * Control codes have their own index
             * Bytes get their own index
             * And words like "Cat" get their own index. 
             * Characters like [...] are part of a word. So you'll 
             * get indexes that have "Hmm[...].
             */
            string[] a_chunks = (new Regex(@"(\(.*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})|(\<.*?\>)|(\[LINE\])|(\[NEXT\])|(\[STOP\])")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();

            int[] a_chunk_length = new int[a_chunks.Length];

            for (int i = 0; i < a_chunks.Length; i++)
            {
                string ChunkToInsert = a_chunks[i];

                if (ChunkToInsert[0] == Constants.ACTION1)
                {
                    //determine how much space a controlcode takes up.
                    a_chunk_length[i] = 0;

                }
                else if (ChunkToInsert[0] == '<')
                {
                    a_chunk_length[i] += 0;
                }
                else if (ChunkToInsert[0] != Constants.BYTE1)
                {
                    /*
                     * Have each index in SubChunk contains a single character.
                     * Stuff in brackets count as a single character. 
                     * 
                     * For example:
                     * 
                     * Full word: "Hmm[...]"
                     * 
                     * Becomes:
                     * 
                     * [0]=H
                     * [1]=m
                     * [2]=m
                     * [3]=[...]
                     * 
                     * This makes the code simple. 
                     */
                    string[] a_SubChunk = (new Regex(@"(\[.*?\])|(.)")).Split(ChunkToInsert).Where(s => s != string.Empty).ToArray();

                    foreach (string CharacterToGetWidthOf in a_SubChunk)
                    {
                        if (chara.GetCHRByteValue(CharacterToGetWidthOf, out byte[] hexid))
                        {
                            if (CHR_Lengthlookup.ContainsKey(hexid))
                            {
                                a_chunk_length[i] += CHR_Lengthlookup[hexid];
                            }
                            else
                            {

                                throw new Exception($"Length calc lookup key does not exist: {MyMath.decToHex(hexid)}.");
                            }
                        }
                        else
                        {
                            throw new Exception($"Entry no #{EntryNumber}. Unable to insert the following string: \"{CharacterToGetWidthOf}\".");
                        }
                    }
                }
            }

            chara.GetCHRByteValue(Constants.SPACE, out byte[] space_id);
            int WidthOfSpace = CHR_Lengthlookup[space_id];

            int Original_PixelsPerLine = PixelsPerLine;
            bool WillLineBreak = false;
            int LineLength = 0;
            bool DelayedSpaceInsertion = false;

            int MaxLineNo = 5;
            int CurrentLine = 1;

            bool NewTextboxArea = false;
            bool previousEdgeCase = false;

            void PadOutLine()
            {
                int NumberOfNewlinesToAdd = MaxLineNo - CurrentLine;

                for (int k = 0; k < NumberOfNewlinesToAdd; k++)
                {
                    NewScriptEntry = string.Concat(NewScriptEntry, "[LINE]");
                }
                CurrentLine = 1;
            }

            //Match on control codes and byte data.
            Regex regex = new Regex(@"(\(.*?\))|({.*?})");

            for (int i = 0; i < a_chunks.Length; i++)
            {
                void IncrementCurrentLine()
                {
                    CurrentLine++;
                    if (CurrentLine == MaxLineNo)
                    {
                        CurrentLine = 1;
                    }

                    if (CurrentLine == 4)
                    {
                        PixelsPerLine = Original_PixelsPerLine - 8; //to adjust for cursor
                    }
                    else
                    {
                        PixelsPerLine = Original_PixelsPerLine;
                    }
                }

                string ChunkToInsert = a_chunks[i];

                if (DialogueEntry == @"MIHO:[LINE]LET'S WHISPER TO EACH OTHER.(PAGE)A: ISN'T IT WEIRD SEEING YOUR OWN MOVIE?[LINE]B: CAN WE TALK ONCE IT'S OVER?[STOP]")
                {
                    if (ChunkToInsert == "[LINE]")
                    {

                    }

                }

                if (ChunkToInsert == "[STOP]" && LineAfterwards)
                {
                    PadOutLine();
                }

                if (ChunkToInsert == "[LINE]")
                {
                    if (previousEdgeCase)
                    {
                        previousEdgeCase = false;
                        continue;
                    }

                    WillLineBreak = true;
                    DelayedSpaceInsertion = false;
                }
                else if (ChunkToInsert == "[NEXT]")
                {
                    LineLength = 0;
                }
                else if (ChunkToInsert == "(PAGE)")
                {
                    LineLength = 0;
                    CurrentLine = 0;
                    PixelsPerLine = Original_PixelsPerLine;
                }

                previousEdgeCase = false;

                bool EdgeCase = LineLength + a_chunk_length[i] == PixelsPerLine && !NewTextboxArea;

                if (LineLength + a_chunk_length[i] > PixelsPerLine)
                {
                    WillLineBreak = true;
                }

                //Remove a space at the end of a line if we're linebreaking.
                if (WillLineBreak && NewScriptEntry.Length > 0 && NewScriptEntry[NewScriptEntry.Length - 1].ToString() == Constants.SPACE)
                {
                    int j = i;
                    while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && a_chunks[j] != "[LINE]") { }

                    if (j < a_chunks.Length && a_chunks[j] == "[LINE]")
                    {
                        NewScriptEntry = NewScriptEntry.Substring(0, NewScriptEntry.Length - 1);
                        LineLength -= WidthOfSpace;
                    }
                }

                if (!WillLineBreak && AutoEdgeLinebreak && ChunkToInsert != Constants.SPACE && EdgeCase)
                {
                    WillLineBreak = true;
                }

                if (WillLineBreak)
                {
                    if (!(EdgeCase))
                    {
                        if (NewScriptEntry.EndsWith(Constants.SPACE))
                        {
                            NewScriptEntry = NewScriptEntry.TrimEnd();
                            LineLength -= WidthOfSpace;
                        }
                    }

                    /*
                     * Don't add a linebreak if there's nothing ahead but control codes at the end of the speaker's line.
                     */
                    int j = i - 1;
                    while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && a_chunks[j] != "[LINE]") { }

                    //Game already autolinebreaks in right on the edge.
                    if (AutoEdgeLinebreak && EdgeCase)
                    {
                        WillLineBreak = false;
                        LineLength = -a_chunk_length[i];
                        IncrementCurrentLine();
                        previousEdgeCase = true;
                    }
                    else if (j < a_chunks.Length)
                    {
                        if (!NewTextboxArea)
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, "[LINE]");
                            IncrementCurrentLine();
                        }
                        else
                        {
                            PadOutLine();
                            NewTextboxArea = false;
                        }

                        LineLength = 0;
                    }

                    WillLineBreak = false;
                }

                if (ChunkToInsert != "[LINE]")
                {
                    /*
                     * Delaying the space insertion makes it easier to determine when
                     * we have a trailing space at the end of a line.
                     * 
                     * For example, this is easier to determine that
                     * we need to remove an extra space:
                     * "(CONTROL CODE) "
                     * 
                     * This is not:
                     * " (CONTROL CODE)"
                     */

                    if (ChunkToInsert == Constants.SPACE && i + 1 < a_chunk_length.Length && regex.Match(a_chunks[i + 1]).Success)
                    {
                        DelayedSpaceInsertion = true;
                    }
                    else
                    {
                        //Not sure why there's two white space removers (one above), but I'll leave this alone now.
                        if (!(LineLength == 0 && ChunkToInsert == Constants.SPACE))
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, ChunkToInsert);
                            LineLength += a_chunk_length[i];
                        }

                        if (DelayedSpaceInsertion && i + 1 < a_chunk_length.Length && !regex.Match(a_chunks[i + 1]).Success)
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, Constants.SPACE);
                            LineLength += WidthOfSpace;
                            DelayedSpaceInsertion = false;
                        }
                    }
                }
            }
        }

        if (NewScriptEntry.Contains('<'))
        {
            throw new Exception();
        }


        if (DialogueEntry == "MIZUHO'S IN FRONT OF THE PIANO.(PAGE)MIZUHO:[LINE]OH, [NAME]. THANKS FOR CALLING ME YESTERDAY.[STOP]")
        {

        }


        if (DialogueEntry == "YOU DON'T HAVE ANY ITEMS.[STOP]")
        {

        }

        return NewScriptEntry;
    }

    public string MetalSlader_Trans_AddLineBreaks(int EntryNumber, string DialogueEntry, int PixelsPerLine, MyDictionary chara, bool InGameAutotLineBreak = false)
    {
        //Split the dialogue so my control codes are in one index while the dialogue is 
        string[] Spiro_controlCode = DialogueEntry.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
        DialogueEntry = Regex.Replace(DialogueEntry, @"\\.*?/", "");

        /*
         * Parse my own control codes.
         * LineWidth = Skips the linebreaking code entirely
         * LineBreakSkip = let's you set the width of the textbox
         */

        if (EntryNumber == 2227)
        {

        }

        bool OverrideLineSkip = false;
        string NewScriptEntry = "";
        bool portraitAlreadyOpen = false;

        if (Spiro_controlCode.Length > 1)
        {
            foreach (string s in Spiro_controlCode)
            {
                if (s.StartsWith("LineWidth="))
                {
                    PixelsPerLine = int.Parse(s.Split('=')[1]);
                }
                else if (s.Equals("LineBreakSkip"))
                {
                    NewScriptEntry = DialogueEntry;
                    OverrideLineSkip = true;
                    break;
                }
                else if (s.Equals("LineWidthPortraitShowing"))
                {
                    portraitAlreadyOpen = true;
                    break;
                }
            }
        }

        if (!OverrideLineSkip)
        {
            const string
                CODE_PORTRAIT_OPEN = "(CODE 10 Portrait)",
                CODE_PORTRAIT_CLOSE = "(CODE 11)",
                CODE_ENDQUOTE = "(End quote)",
                CODE_WAIT = "(WAIT)",
                CODE_NAMECARD_COLON = ":";

            const int LENGTH_OF_CURSOR = 8;
            const int LENGTH_OF_PORTRAINT = 64;

            /*
             * Split the dialogue string into chunks that we can work with.
             * Space get their own entries.
             * 
             * Control codes have their own index
             * Bytes get their own index
             * And words like "Cat" get their own index. 
             * Characters like [...] are part of a word. So you'll 
             * get indexes that have "Hmm[...].
             */
            string[] a_chunks;
            if (InGameAutotLineBreak)
            {
                a_chunks = (new Regex(@"(\((?!End quote).*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();
            }
            else
            {
                a_chunks = (new Regex(@"(\(.*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();
            }

            int[] a_chunk_length = new int[a_chunks.Length];

            for (int i = 0; i < a_chunks.Length; i++)
            {
                string ChunkToInsert = a_chunks[i];

                if (ChunkToInsert[0] == Constants.ACTION1)
                {
                    if (!chara.Dict_Sort_By_Character_With_String_Val.ContainsKey(ChunkToInsert))
                    {
                        throw new Exception($"The following is not a valid control code: {ChunkToInsert}.");
                    }

                    //determine how much space a controlcode takes up.
                    a_chunk_length[i] = ControlCode_Lengthlookup[chara.Dict_Sort_By_Character_With_String_Val[ChunkToInsert][0].Key];
                }
                else if (ChunkToInsert[0] != Constants.BYTE1)
                {
                    /*
                     * Have each index in SubChunk contains a single character.
                     * Stuff in brackets count as a single character. 
                     * 
                     * For example:
                     * 
                     * Full word: "Hmm[...]"
                     * 
                     * Becomes:
                     * 
                     * [0]=H
                     * [1]=m
                     * [2]=m
                     * [3]=[...]
                     * 
                     * This makes the code simple. 
                     */
                    string[] a_SubChunk = (new Regex(@"(\(.*?\))|(\[.*?\])|(.)")).Split(ChunkToInsert).Where(s => s != string.Empty).ToArray();

                    foreach (string CharacterToGetWidthOf in a_SubChunk)
                    {
                        if (CharacterToGetWidthOf[0] == Constants.ACTION1)
                        {
                            if (!chara.Dict_Sort_By_Character_With_String_Val.ContainsKey(CharacterToGetWidthOf))
                            {
                                throw new Exception($"The following is not a valid control code: {CharacterToGetWidthOf}.");
                            }

                            //determine how much space a controlcode takes up.
                            a_chunk_length[i] += ControlCode_Lengthlookup[chara.Dict_Sort_By_Character_With_String_Val[CharacterToGetWidthOf][0].Key];
                        }
                        else if (chara.GetCHRByteValue(CharacterToGetWidthOf, out byte[] hexid))
                        {
                            a_chunk_length[i] += CHR_Lengthlookup[hexid];
                        }
                        else
                        {
                            throw new Exception($"Entry no #{EntryNumber}. Unable to insert the following string: \"{CharacterToGetWidthOf}\".");
                        }
                    }
                }
            }


            chara.GetCHRByteValue(Constants.SPACE, out byte[] space_id);
            int WidthOfSpace = CHR_Lengthlookup[space_id];

            int CurrentLine = 1;
            int Original_PixelsPerLine = PixelsPerLine;
            bool WillLineBreak = false;
            int LineLength = 0;
            bool DelayedSpaceInsertion = false;
            bool WaitFlag = false;
            bool PortraitDisplay = false;
            bool AdjustForEndquote = false;
            bool LastWordChunk = false;
            int LastChunkLength = 0;

            //Match on control codes and byte data.

            Regex regex;

            if (InGameAutotLineBreak)
            {
                regex = new Regex(@"(\((?!End quote).*?\))|({.*?})");
            }
            else
            {
                regex = new Regex(@"(\(.*?\))|({.*?})");
            }

            for (int i = 0; i < a_chunks.Length; i++)
            {
                if (portraitAlreadyOpen)
                {
                    PortraitDisplay = true;
                    PixelsPerLine = Get_LengthOfTextbox();
                    portraitAlreadyOpen = false;
                }

                int Get_LengthOfTextbox()
                {
                    int TextboxLength = Original_PixelsPerLine;

                    /*
                     * The wait cursor decrement doesn't stack with the portrait 
                     * length decrement.
                     * 
                     * You see, you can't actually write in the column where the cursor
                     * would be when a portrait is displayed.
                     */
                    if (PortraitDisplay)
                    {
                        TextboxLength = Original_PixelsPerLine - LENGTH_OF_PORTRAINT;
                    }
                    else if (WaitFlag)
                    {
                        TextboxLength = Original_PixelsPerLine - LENGTH_OF_CURSOR;
                    }

                    if (AdjustForEndquote)
                    {
                        TextboxLength -= 8;
                    }


                    return TextboxLength;
                }

                string ChunkToInsert = a_chunks[i];

                if (InGameAutotLineBreak && (LineLength == 0 && i != 0 && (ChunkToInsert == Constants.SPACE || ChunkToInsert == Constants.NEWLINE)))
                {
                    continue;
                }


                LastWordChunk = false;
                LastChunkLength = 0;

                if (EntryNumber == 1081 && ChunkToInsert.Contains("Himukai."))
                {

                }

                /*
                 * Did we process the wait control code?
                 * If we did, set the textbox width back,
                 * we are no longer factoring in the "wait cursor"
                 */
                if (WaitFlag)
                {
                    WaitFlag = false;
                    PixelsPerLine = Get_LengthOfTextbox();
                }

                if (AdjustForEndquote)
                {
                    AdjustForEndquote = false;
                    PixelsPerLine = Get_LengthOfTextbox();
                }

                if (!WaitFlag && CurrentLine > 3)
                {
                    //Look ahead and see if there's a (WAIT) control code ahead.
                    int j = i;
                    while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && a_chunks[j] != CODE_WAIT && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE) { }

                    if (j < a_chunks.Length && a_chunks[j] == CODE_WAIT)
                    {
                        //Reduce the max number of pixels per line by the arrows width.
                        //Otherwise the text would overlap with the next arrow.
                        WaitFlag = true;
                        PixelsPerLine = Get_LengthOfTextbox();
                    }
                }

                /*
                 * Do we have the have's own autolinebreaking code enabled.
                 * (The code sucks and actually restricts how 
                 * much text you can have onscreen)
                 */

                if (InGameAutotLineBreak)
                {
                    if (ChunkToInsert.Contains(CODE_ENDQUOTE))
                    {
                        AdjustForEndquote = true;
                        PixelsPerLine = Get_LengthOfTextbox();
                    }
                    else if (!ChunkToInsert.Contains(Constants.SPACE))
                    {
                        int j = i;
                        while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && !a_chunks[j].Contains(CODE_ENDQUOTE) && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE) { }


                        if (j < a_chunks.Length && a_chunks[j].Contains(CODE_ENDQUOTE))
                        {
                            AdjustForEndquote = true;
                            PixelsPerLine = Get_LengthOfTextbox();
                        }
                    }
                }

                if (ChunkToInsert[0] == Constants.ACTION1)
                {
                    if (ChunkToInsert.Contains(CODE_NAMECARD_COLON) || ChunkToInsert.Contains(CODE_ENDQUOTE))
                    {
                        LineLength = 0;
                        DelayedSpaceInsertion = false;
                    }

                    if (ChunkToInsert.Contains(CODE_ENDQUOTE))
                    {
                        CurrentLine++;
                    }

                    if (ChunkToInsert == Constants.NEWLINE)
                    {
                        LineLength = 0;
                        CurrentLine++;
                        DelayedSpaceInsertion = false;
                    }

                    if (ChunkToInsert == CODE_PORTRAIT_OPEN)
                    {
                        PortraitDisplay = true;
                        PixelsPerLine = Get_LengthOfTextbox();
                        CurrentLine = 1;
                    }
                    else if (ChunkToInsert == CODE_PORTRAIT_CLOSE)
                    {
                        PortraitDisplay = false;
                        PixelsPerLine = Get_LengthOfTextbox();
                        CurrentLine = 1;
                    }
                }

                /*
                 * Check if a word is split by a control code.
                 * 
                 * If it is, see if we need to linebreak.
                 */
                if (!WillLineBreak && ChunkToInsert != Constants.SPACE && ChunkToInsert[0] != Constants.ACTION1 && ChunkToInsert[0] != Constants.BYTE1)
                {
                    int j = i - 1;

                    int LengthOfFullWord = 0;
                    while (++j < a_chunks.Length && a_chunks[j] != Constants.SPACE && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE)
                    {
                        LengthOfFullWord += a_chunk_length[j];
                    }

                    if (j == a_chunks.Length)
                    {
                        LastWordChunk = true;
                        LastChunkLength = LengthOfFullWord;
                    }


                    if (LengthOfFullWord > a_chunk_length[i])
                    {
                        if (LineLength + LengthOfFullWord > PixelsPerLine)
                        {
                            LineLength = 0;
                            WillLineBreak = true;
                        }
                    }
                }

                if (!WillLineBreak)
                {
                    int u = i;

                    if (!regex.Match(a_chunks[u]).Success)
                    {
                        //Check to see if we didn't already identify 
                        //it's the last chunk previously in function (when we were checking for a word 
                        //split by control code(s).
                        if (!LastWordChunk)
                        {
                            LastChunkLength += a_chunk_length[u];

                            while (++u < a_chunks.Length && regex.Match(a_chunks[u]).Success && a_chunks[u] != Constants.SPACE && a_chunks[u] != Constants.NEWLINE)
                            {
                                LastChunkLength += a_chunk_length[u];
                            }

                            if (u == a_chunks.Length)
                            {
                                LastWordChunk = true;
                            }
                        }
                        else
                        {
                            if (EntryNumber == 1511 && ChunkToInsert.Contains("right"))
                            {

                            }
                        }
                    }

                    bool NormalLineBreak = (LineLength + a_chunk_length[i] > PixelsPerLine);

                    //Kind of a bad way to check if this is the final chunk with
                    //text to insert.
                    if (!NormalLineBreak && (CurrentLine >= 3 && !WaitFlag && LastWordChunk && !PortraitDisplay))
                    {
                        if (LineLength + LastChunkLength > PixelsPerLine - LENGTH_OF_CURSOR)
                        {
                            if (Global.Verbose)
                            {
                                Console.WriteLine($"Autolinebreak: end of line; Entryno: {EntryNumber}.");
                            }
                            WillLineBreak = true;
                        }
                    }
                    else
                    {
                        if (NormalLineBreak)
                        {
                            WillLineBreak = true;
                        }
                    }
                }

                //Remove a space at the end of a line if we're linebreaking.
                if (WillLineBreak && NewScriptEntry.Length > 0 && NewScriptEntry[NewScriptEntry.Length - 1].ToString() == Constants.SPACE)
                {
                    int j = i;
                    while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && a_chunks[j] != Constants.NEWLINE) { }

                    if (j < a_chunks.Length && a_chunks[j] == Constants.NEWLINE)
                    {
                        NewScriptEntry = NewScriptEntry.Substring(0, NewScriptEntry.Length - 1);
                        LineLength -= WidthOfSpace;
                    }
                }

                if (WillLineBreak)
                {
                    if (NewScriptEntry.EndsWith(Constants.SPACE))
                    {
                        NewScriptEntry = NewScriptEntry.TrimEnd();
                    }

                    /*
                     * Don't add a linebreak if there's nothing ahead but control codes at the end of the speaker's line.
                     */
                    int j = i - 1;
                    while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE && a_chunks[j].Contains(CODE_ENDQUOTE)) { }

                    if (j < a_chunks.Length && !a_chunks[j].Contains(CODE_NAMECARD_COLON))
                    {
                        NewScriptEntry = string.Concat(NewScriptEntry, Constants.NEWLINE);
                        LineLength = 0;
                        CurrentLine++;
                    }

                    WillLineBreak = false;
                }

                if (InGameAutotLineBreak && (LineLength + a_chunk_length[i] == PixelsPerLine))
                {
                    LineLength = 0 - a_chunk_length[i];
                }

                if (!(LineLength == 0 && ChunkToInsert == Constants.SPACE))
                {
                    /*
                     * Delaying the space insertion makes it easier to determine when
                     * we have a trailing space at the end of a line.
                     * 
                     * For example, this is easier to determine that
                     * we need to remove an extra space:
                     * "(CONTROL CODE) "
                     * 
                     * This is not:
                     * " (CONTROL CODE)"
                     */

                    if (ChunkToInsert == Constants.SPACE && i + 1 < a_chunk_length.Length && regex.Match(a_chunks[i + 1]).Success)
                    {
                        DelayedSpaceInsertion = true;
                    }
                    else
                    {
                        NewScriptEntry = string.Concat(NewScriptEntry, ChunkToInsert);
                        LineLength += a_chunk_length[i];

                        if (DelayedSpaceInsertion && i + 1 < a_chunk_length.Length && !regex.Match(a_chunks[i + 1]).Success)
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, Constants.SPACE);
                            LineLength += WidthOfSpace;
                            DelayedSpaceInsertion = false;
                        }

                        if (ChunkToInsert.Contains(CODE_ENDQUOTE))
                        {
                            LineLength = 0;
                            DelayedSpaceInsertion = false;
                        }
                    }
                }
            }
        }

        if (EntryNumber == 1081)
        {

        }

        return NewScriptEntry;
    }

    /// <summary>
    /// Gets the pixel length for dictionary values. Useful only for control codes that have text behind them.
    /// 
    /// It writes non-control code entries as well. So some clockcycle are wasted.
    /// 
    /// Keep in mind, the text in the control codes can't be broken up between lines.
    /// </summary>
    /// <param name="line"></param>
    /// <param name="chara"></param>
    public void AddDictionaryEntryLength(string line, MyDictionary chara)
    {
        int LengthOfDictionary = 0;
        line = Regex.Replace(line, "{.*?}", "");

        for (int i = 0; i < line.Length; i++)
        {
            string Character;
            int length = 0;
            byte[] possibleKey;
            do
            {
                if (++length > line.Length - i)
                {
                    throw new Exception();
                }

                Character = line.Substring(i, length);
            }
            while (!chara.GetCHRByteValue(Character, out possibleKey));

            if (!CHR_Lengthlookup.ContainsKey(possibleKey))
            {
                throw new Exception($"CHR key not in length list: $#{MyMath.getValueOfBytes(possibleKey):X2}. Make sure the value is in the length file.");
            }

            LengthOfDictionary += CHR_Lengthlookup[possibleKey];
            i += Character.Length - 1;
        }

        ControlCode_Lengthlookup.Add(DictionaryKeys.Dequeue(), LengthOfDictionary);
    }

    //From the dictionary class more or less.
    private byte[] GetHexValueFromFileLine(string str)
    {
        int index = str.IndexOf(Constants.SEPERATOR);
        return MyMath.hexToBytes(str.Substring(0, index));
    }

    /// <summary>
    /// The argument a line from the length file.
    /// This file stores the lengths of each 
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private int ParseLengthFileLine(string str)
    {
        int index = str.IndexOf(Constants.SEPERATOR);
        return int.Parse(str.Substring(index + Constants.SEPERATOR.Length).ToString());
    }
}
