﻿using System;
using System.Runtime.InteropServices;

class AddressHiRom
{
    //by: darkmoon
    public static int snesToPc(int addr)
    {
        if ((addr & 0x400000) == 0)
        {
            addr &= 0x3FFFFF;
        }
        else
        {
            if ((addr & 0x8000) == 0)
            {
                addr &= 0x3FFFFF;
            }
            else
            {
                throw new Exception($"Invalid PC address {addr:X2} for HiROM.");
            }           
        }
        return addr;
    }
}