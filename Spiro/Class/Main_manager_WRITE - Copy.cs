﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using static Enums;

public partial class Main_manager
{
    public string
        Original,
        Comment,
        New,
        cypro_path,
        Byte_dump,
        Menu;

    public Main_manager(string MAIN)
    {
        Global.MainFolder = MAIN;
        Original = Path.Combine(Global.MainFolder, "Original.xml");
        Comment = Path.Combine(Global.MainFolder, "Comment.xml");
        New = Path.Combine(Global.MainFolder, "New.xml");
        cypro_path = Path.Combine(Global.MainFolder, "Main.cypro");
        Byte_dump = Path.Combine(Global.MainFolder, "ByteDump.xml");
        Menu = Path.Combine(Global.MainFolder, "Menu.xml");

        Settings.ReadSettingsFile(cypro_path);
    }

    public void WriteScriptToROM()
    {
        //read ROM
        byte[] RomBinaryData = File.ReadAllBytes(Settings.ROMToCopy);

        ROM.SetRom(RomBinaryData, out bool isHeadered);

        //read settings file.

        PointerManager Ptr_reader = new PointerManager();

        /*
         * Load in the CHR table, until we find the dictionary and load it in.
         */
        MonologueIO mono = null;

        List<List<byte[]>> Script_Data = new List<List<byte[]>>();
        List<List<int>> Master_Script_Length = new List<List<int>>();

        XElement x = XDocument.Load(New, LoadOptions.PreserveWhitespace).Element(Constants.ROOT);
        Stopwatch s = Stopwatch.StartNew();

        int IndexTable = 0;

        DictionaryTextfileIO dictio = null;

        WidthCalc length_calc = new WidthCalc();

        int Byte_Size = 0;
        int CharacterSize = 0;
        int BytesLost = 0;

        foreach (XElement Script_Entry in x.Elements())
        {
            Console.WriteLine($"Encoding indextable: {IndexTable}");

            /*
             * CHR & Dictionary management start
             */
            string CHRFile = Settings.Pointer_Write_info[IndexTable].CHRFile;
            string DictionaryTemplateFile = Settings.Pointer_Write_info[IndexTable].DictionaryTemplateFile;
            int PixelsPerLine = Settings.Pointer_Write_info[IndexTable].PixelsPerLine;
            bool InserLineBreaks = Settings.Pointer_Write_info[IndexTable].InserLineBreaks;



            if (Settings.Pointer_Write_info[IndexTable].Name.Contains("Ending"))
            {

            }


            if (!string.IsNullOrEmpty(CHRFile))
            {
                string NewCHR = string.Format(Constants.Path_To_x_trans, CHRFile);

                //If the CHR file changes.
                if (NewCHR != Global.Path_To_CHR_trans)
                {
                    //Update the file path for the CHR translation
                    Global.Path_To_CHR_trans = string.Format(Constants.Path_To_x_trans, CHRFile);

                    //If we're autoline breaking.
                    if (PixelsPerLine != Constants.PIXEL_LENGTH_NULL)
                    {
                        //Load CHR data into length_calc
                        length_calc.LoadCHR(CHRFile);
                    }
                }
            }

            if (!string.IsNullOrEmpty(DictionaryTemplateFile))
            {
                //check if file is already open.
                if (Global.Path_To_Dict_trans != string.Format(Constants.Path_To_x_trans, DictionaryTemplateFile))
                {
                    Global.Path_To_Dict_trans = string.Format(Constants.Path_To_x_trans, DictionaryTemplateFile);
                    Global.Path_To_Dict_Template = string.Format(Constants.Path_To_dict_template, DictionaryTemplateFile);
                    dictio = new DictionaryTextfileIO(Global.Path_To_Dict_trans);

                    mono = new MonologueIO(LoadDict: true, ReadOriginal: false);
                    length_calc.LoadDictionaryTemplate(DictionaryTemplateFile);
                }
            }
            else
            {
                //if we're not using a dictionary template, just use the CHR as a fallback.
                mono = new MonologueIO(LoadDict: false, ReadOriginal: false);
            }

            /*
             * String to byte stream
             */
            bool IsDictionary = Settings.Pointer_Write_info[IndexTable].IsDictionary;

            List<byte[]> Entries = new List<byte[]>();

            int EntryNo = 0;


            foreach (XElement Entry in Script_Entry.Elements())
            {
                List<byte> ScriptAsBytes;

                string Line = Entry.Value;

                if (Settings.Pointer_Write_info[IndexTable].SquishyTextFile != string.Empty)
                {
                    SquishyText squish = new SquishyText(string.Format(Constants.Path_To_x_trans, Settings.Pointer_Write_info[IndexTable].SquishyTextFile), Settings.Pointer_Write_info[IndexTable].SquishyTextFile_useregex);
                    squish.ReadSquishyTextFile();
                    Line = squish.AddSquishyText(Line);
                }

                //auto-linebreak script.
                if (!IsDictionary && InserLineBreaks)
                {
                    switch (Settings.Pointer_Write_info[IndexTable].AutoLineBreak)
                    {
                        case (int)Enums.AutoLineBreak.None:
                            break;
                        case (int)Enums.AutoLineBreak.MetalSladerGlory:
                            Line = length_calc.MetalSlader_Trans_AddLineBreaks(EntryNo, Line, PixelsPerLine, mono.chara);
                            break;
                        case (int)Enums.AutoLineBreak.TokimekiMiho:
                            Line = length_calc.TokimekiMiho_Trans_AddLineBreaks(EntryNo, Line, PixelsPerLine, mono.chara, true);
                            break;
                        case (int)Enums.AutoLineBreak.TokimekiMihoMessage:
                            Line = length_calc.TokimekiMiho_Trans_AddLineBreaks(EntryNo, Line, PixelsPerLine, mono.chara, false);
                            break;
                        default:
                            throw new Exception($"Auto line break. Invalid id: {Settings.Pointer_Write_info[IndexTable].AutoLineBreak}");
                    }
                }

                if (EntryNo == 13 && IndexTable == 2)
                {

                }

                List<byte> ScriptBytes_NoDelimeter;

                switch (Settings.Pointer_Write_info[IndexTable].RenderType)
                {
                    case (int)RenderType.Dictionary:
                        ScriptAsBytes = mono.ConvertScriptToHex_WithDictionary(Line, EntryNo);
                        break;
                    case (int)RenderType.NoDictionary_BigEndian:
                    case (int)RenderType.NoDictionary_MetalSladerNameCard:
                        ScriptAsBytes = mono.ConvertScriptToHex_WithoutDictionary(Line, EntryNo, out ScriptBytes_NoDelimeter, Settings.Pointer_Read_info[IndexTable].Delimeter);

                        //if dictionary, write to dictionary file on computer.
                        if (IsDictionary)
                        {
                            dictio.WriteEntry(ScriptBytes_NoDelimeter);
                        }

                        break;
                    default:
                        throw new Exception();
                }

                Entries.Add(ScriptAsBytes.ToArray());

                if (IsDictionary && PixelsPerLine != Constants.PIXEL_LENGTH_NULL)
                {
                    length_calc.AddDictionaryEntryLength(Entry.Value, mono.chara);
                }

                Byte_Size += ScriptAsBytes.Count;
                CharacterSize += Entry.Value.Length;
                EntryNo++;
            }

            /*
             * If the region is a dictionary table, write to the dictionary file.
             * Why a file? This is so you can set the dictionary file manually
             * and not have the utility read from the rom to get the dictionary.
             */
            if (IsDictionary)
            {
                if (Settings.Pointer_Write_info.Length > IndexTable + 1 && !Settings.Pointer_Write_info[IndexTable + 1].IsDictionary)
                {
                    dictio.Write();
                    mono = new MonologueIO(LoadDict: true, ReadOriginal: false);
                }
            }

            IndexTable++;
            Script_Data.Add(Entries);
        }

        s.Stop();

        if (Global.Verbose)
        {
            Console.WriteLine($"Compressed text to bytes in: {s.ElapsedMilliseconds / 1000} second(s).");
            Console.WriteLine($"Compressed script size: {Byte_Size} bytes. Uncompressed script size: {CharacterSize} characters.");
        }

        /*
        /*
         * We just finished converting the script to byte data. Now let's prepare to
         * write to the ROM.
         */

        object[][] Pointers = new object[Settings.Pointer_Write_info.Length][];

        for (int i = 0; i < Settings.Pointer_Write_info.Length; i++)
        {
            Pointers[i] = new object[Settings.Pointer_Write_info[i].EntryNumber];
        }

        List<byte>[] ByteData = Enumerable.Range(0, Settings.WriteableRange.Length).Select(_ => new List<byte>()).ToArray();

        foreach (Pointer_Write XML_WriteData in Settings.Script_insertion_info)
        {
            int DataType = (int)e_DataType.NoDataTpye;

            int WriteRegion_i = 0;

            ProgressBar progress = new ProgressBar();
            Console.WriteLine();

            int ProgressBarMax = 0;

            foreach (List<byte[]> b in Script_Data)
            {
                ProgressBarMax += b.Count;
            }

            int Current_Progress = 0;

            foreach (int EntryOwner_i in XML_WriteData.EntryOwner)
            {
                Console.WriteLine($"Writing entry #{EntryOwner_i}.");

                bool StoreOutOfOrderToSaveSpace = Settings.Pointer_Write_info[EntryOwner_i].StoreOutOfOrderToSaveSpace;

                Stack<KeyValuePair<int, byte[]>> LinesSortedBySize = new Stack<KeyValuePair<int, byte[]>>();

                ulong Type_10_CurrentAddress = Settings.Pointer_Write_info[EntryOwner_i].Address_of_pointer;
                ulong Type_10_NextAddress = Type_10_CurrentAddress;

                if (StoreOutOfOrderToSaveSpace)
                {
                    Console.WriteLine("Space saving mode activated. Lines will be written out of order from biggest to smallest.");
                    SortedList<int, KeyValuePair<int, byte[]>> linesbysize = new SortedList<int, KeyValuePair<int, byte[]>>(new DuplicateKeyComparer<int>());

                    int id = 0;
                    foreach (byte[] Line in Script_Data[EntryOwner_i])
                    {
                        linesbysize.Add(Line.Length, new KeyValuePair<int, byte[]>(id++, Line));
                    }
                    foreach (KeyValuePair<int, KeyValuePair<int, byte[]>> kvp in linesbysize)
                    {
                        LinesSortedBySize.Push(kvp.Value);
                    }
                }
                else
                {
                    int id = Script_Data[EntryOwner_i].Count - 1;

                    List<byte[]> temp = Script_Data[EntryOwner_i];
                    temp.Reverse();

                    foreach (byte[] Line in temp)
                    {
                        LinesSortedBySize.Push(new KeyValuePair<int, byte[]>(id--, Line));
                    }


                }

                while (LinesSortedBySize.Count != 0)
                {
                    KeyValuePair<int, byte[]> kvp = LinesSortedBySize.Pop();
                    int Linenumber = kvp.Key;
                    byte[] Line = kvp.Value;

                    if (Global.Verbose)
                    {
                        Console.WriteLine($"Line number: {Linenumber}");
                    }

                    /*
                     * The WriteRegion_i is the slot number in XML.
                     * It's not the actual value.
                     * 
                     * Enter the loop if we've run out of space in the write region. 
                     * This is a loop in case the next write region is not big enough 
                     * for some reason.
                     */

                    int Region2Write2 = -1;

                    if (StoreOutOfOrderToSaveSpace)
                    {
                        WriteRegion_i = 0;
                    }

                    while (Settings.WriteableRange[XML_WriteData.WriteableId[WriteRegion_i]].Size < Line.Length + ByteData[XML_WriteData.WriteableId[WriteRegion_i]].Count)
                    {
                        if (Global.Verbose)
                        {
                            Console.WriteLine($"Writable region: {Settings.WriteableRange[WriteRegion_i].Size} < ROM: {Line.Length + ByteData[XML_WriteData.WriteableId[WriteRegion_i]].Count}");
                            Console.WriteLine($"Writable region name: {Settings.WriteableRange[WriteRegion_i].Name}");
                            Console.WriteLine($"Bytes lost: {BytesLost += Line.Length}");
                        }

                        if (++WriteRegion_i >= XML_WriteData.WriteableId.Length)
                        {
                            int FreeSpace = Settings.WriteableRange[XML_WriteData.WriteableId[WriteRegion_i - 1]].Size - ByteData[XML_WriteData.WriteableId[WriteRegion_i - 1]].Count;
                            int LinesLeftToInsert = LinesSortedBySize.Count + 1;
                            int UninsertedDataSize = 0;
                            UninsertedDataSize += kvp.Value.Length;
                            while (LinesSortedBySize.Count != 0)
                            {
                                UninsertedDataSize += LinesSortedBySize.Pop().Value.Length;
                            }

                            throw new Exception(
                                $"Not enough space.\n" +
                                $"Entry owner name:        {Settings.Pointer_Write_info[EntryOwner_i].Name}\n" + 
                                $"Line number:             {Linenumber}/{Script_Data[EntryOwner_i].Count - 1}\n" +
                                $"Lines left:              {LinesLeftToInsert}\n" +
                                $"Write subregion Size:    {Settings.WriteableRange[WriteRegion_i - 1].Size:0000} bytes\n" +
                                $"Size of uninserted data: {UninsertedDataSize:0000} bytes\n" +
                                $"Free space in region:    {FreeSpace:0000} bytes\n" +
                                $"Space overflow:          {UninsertedDataSize - FreeSpace:0000} bytes\n"
                                );
                        }

                        if (Global.Verbose)
                        {
                            Console.WriteLine($"Write region: {WriteRegion_i}");
                        }
                    }

                    Region2Write2 = XML_WriteData.WriteableId[WriteRegion_i];

                    /*
                     * Convert address
                     */

                    ulong PC_Address = (ulong)ByteData[Region2Write2].Count + Settings.WriteableRange[Region2Write2].StartAddress;
                    ulong Converted_address = PC_Address;
                    byte[] ByteArray = null;

                    switch (Settings.Pointer_Write_info[EntryOwner_i].AddressConversion)
                    {
                        case (int)Enums.AddressConversion.None:
                            break;
                        case (int)Enums.AddressConversion.LOROM:
                            Converted_address = (ulong)AddressLoROM.pcToSnes((int)PC_Address);
                            break;
                        default:
                            throw new Exception();
                    }

                    /*ulong Reduction(ulong Address)
                    {
                        int internalPCDifference = Settings.Pointer_Write_info[EntryOwner_i].PCDifference;

                        if (Settings.Pointer_Write_info[EntryOwner_i].PCDifference_Adds)
                        {
                            internalPCDifference *= -1;
                        }

                        return Address += internalPCDifference;
                    }*/

                    void AddTOAddress()
                    {
                        int PCDifference = Settings.Pointer_Write_info[EntryOwner_i].PCDifference;

                        if (Settings.Pointer_Write_info[EntryOwner_i].PCDifference_Adds)
                        {
                            PCDifference *= -1;
                        }

                        if (Converted_address + (ulong)PCDifference < 0)
                        {
                            throw new Exception($"Address negative after subtraction. {Converted_address} + {PCDifference} = {Converted_address + (ulong)PCDifference}");
                        }

                        Converted_address = (ulong)(PCDifference + (int)Converted_address);
                    }

                    AddTOAddress();

                    if (Linenumber == 1187)
                    {

                    }

                    switch (Settings.Pointer_Write_info[EntryOwner_i].PrimaryPointerType)
                    {
                        case (int)Enums.PointerType.None:
                            break;
                        case (int)Enums.PointerType.LittleEndian:
                            ByteArray = Ptr_reader.GetLittleEndian(Converted_address, Settings.Pointer_Write_info[EntryOwner_i].PrimaryPointerLength);
                            DataType = (int)e_DataType.ByteArray;
                            break;
                        case (int)Enums.PointerType.Three_Byte_Metal_Slader_DX_Primary_NoLength:
                            /*
                             * Bits for this address are zeroed out when you're using a delimeter,
                             * so we pass the length as zero here.
                             */
                            Converted_address = Ptr_reader.CreateAddress_MetalSlader_SNES(Converted_address, 0);
                            DataType = (int)e_DataType.IntAddress;
                            break;
                        case (int)Enums.PointerType.Three_Byte_Metal_Slader_DX_Primary:
                            Converted_address = Ptr_reader.CreateAddress_MetalSlader_SNES(Converted_address, Line.Length);
                            DataType = (int)e_DataType.IntAddress;
                            break;
                        case (int)Enums.PointerType.Three_Byte_MetalSlader_NES_Primary_NoLength:
                            Converted_address = Ptr_reader.CreateAddress_MetalSlader_NES(Converted_address, 0);
                            DataType = (int)e_DataType.IntAddress;
                            break;
                        case (int)Enums.PointerType.Three_Byte_MetalSlader_NES_Primary:
                            Converted_address = Ptr_reader.CreateAddress_MetalSlader_NES(Converted_address, Line.Length);
                            DataType = (int)e_DataType.IntAddress;
                            break;
                        case (int)Enums.PointerType.Three_Byte_MetalSlader_NES_trans_Primary_NoLength:
                            Converted_address = Ptr_reader.CreateAddress_MetalSlader_NES(Converted_address, 0, true);
                            DataType = (int)e_DataType.IntAddress;
                            break;
                        case (int)Enums.PointerType.Arle_Adventure_GBC_7_Byte_Pointer:
                            throw new Exception();
                        case (int)Enums.PointerType.NoDelimeterNoPointer_Fixed:
                        case (int)Enums.PointerType.NoPointer_Delimeter:
                            DataType = (int)e_DataType.NoDataTpye;
                            break;
                        case (int)Enums.PointerType.Custom:
                            ByteArray = Ptr_reader.CreateAddress_CustomFormat(Converted_address, Settings.Pointer_Write_info[EntryOwner_i].SpecialPointerFormat);
                            DataType = (int)e_DataType.ByteArray;
                            break;

                        default:
                            throw new Exception();
                    }

                    switch (DataType)
                    {
                        case (int)e_DataType.NoDataTpye:
                            break;
                        case (int)e_DataType.IntAddress:
                            Pointers[EntryOwner_i][Linenumber] = Converted_address;
                            break;
                        case (int)e_DataType.ByteArray:
                            Pointers[EntryOwner_i][Linenumber] = ByteArray;
                            break;
                        default:
                            throw new Exception($"No pointer data type. Pointer type: {Settings.Pointer_Write_info[EntryOwner_i].PrimaryPointerType}.");
                    }

                    ByteData[Region2Write2] = ByteData[Region2Write2].Concat(Line).ToList();
                    Linenumber++;
                    progress.Report((double)Current_Progress++ / ProgressBarMax);
                }
            }

            //now write the pointers
            foreach (int EntryNo in XML_WriteData.EntryOwner)
            {
                int PointerLength = Settings.Pointer_Write_info[EntryNo].PrimaryPointerLength;
                object[] PointerTable = Pointers[EntryNo];

                switch (DataType)
                {
                    case (int)e_DataType.NoDataTpye:
                        break;
                    case (int)e_DataType.IntAddress:
                        for (int i = 0; i < PointerTable.Length; i++)
                        {
                            /*
                             * We have to manipulate the bitconverter output array because it 
                             * a) Reversed the byte order.
                             * b) Convert it to a X length pointer. Bitconverter outputs 4 bytes (an int),
                             *    so we need to cut it as needed.
                             */

                            if (PointerTable[i] == null)
                            {
                                throw new Exception($"Entry #{i} doesn't exist in XML file.");
                            }

                            byte[] PointerAsBytes = BitConverter.GetBytes((ulong)PointerTable[i]).Reverse().ToArray();
                            byte[] Truncated = new byte[Settings.Pointer_Write_info[EntryNo].PrimaryPointerLength];
                            Array.Copy(PointerAsBytes, PointerAsBytes.Length - Truncated.Length, Truncated, 0, Truncated.Length);
                            Array.Copy(Truncated, 0, ROM.DATA, (int)Settings.Pointer_Write_info[EntryNo].Address_of_pointer + PointerLength * i, PointerLength);
                        }

                        break;
                    case (int)e_DataType.ByteArray:

                        for (int i = 0; i < PointerTable.Length; i++)
                        {
                            byte[] b = (byte[])PointerTable[i];

                            Array.Copy(b, 0, ROM.DATA, (int)Settings.Pointer_Write_info[EntryNo].Address_of_pointer + PointerLength * i, PointerLength);
                        }
                        break;
                }
            }
        }

        int FreespaceCount = 0;
        int WastedSpaceCount = 0;
        //Write region to the ROM
        for (int i = 0; i < Settings.WriteableRange.Length; i++)
        {
            //fill the data with zeros

            int initialspace = ByteData[i].Count;

            while (Settings.WriteableRange[i].Size > ByteData[i].Count)
            {
                ByteData[i].Add(Constants.FREESPACE_byte);
                FreespaceCount++;
                if (initialspace != 0)
                {
                    WastedSpaceCount++;
                }
            }

            if (Settings.WriteableRange[i].Size != ByteData[i].Count)
            {
                throw new Exception();
            }

            if (ROM.DATA.Length < (int)Settings.WriteableRange[i].StartAddress)
            {
                throw new Exception($"ROM address to write to, 0x{Settings.WriteableRange[i].StartAddress:X2}, is out of bounds.");
            }

            if (ROM.DATA.Length < (int)Settings.WriteableRange[i].StartAddress + Settings.WriteableRange[i].Size)
            {
                throw new Exception("Write size too big for ROM.");
            }

            //add data to writable regions
            Array.Copy(ByteData[i].ToArray(), 0, ROM.DATA, (int)Settings.WriteableRange[i].StartAddress, Settings.WriteableRange[i].Size);
        }

        if (Global.Verbose)
        {
            Console.WriteLine($"Free space: {FreespaceCount}");
            Console.WriteLine($"Wasted space: {WastedSpaceCount}");
        }

    }
}