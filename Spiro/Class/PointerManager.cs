using System;
using System.Collections.Generic;
using System.Linq;

public class PointerManager
{
    public ulong[][] Master_Primary_Pointer_adrresses { get; set; }
    public ulong[][] Master_Secondary_Pointer_adrresses { get; set; }
    public int[][] Master_Length { get; set; }

    public PointerManager()
    {
        Master_Primary_Pointer_adrresses = new ulong[Settings.Pointer_Read_info.Length][];
        Master_Secondary_Pointer_adrresses = new ulong[Settings.Pointer_Read_info.Length][];
        Master_Length = new int[Settings.Pointer_Read_info.Length][];
    }

    public ulong CreateAddress_MetalSlader_SNES(ulong PC_Address, int Length)
    {
        ulong SnesAddress = PC_Address;

        byte byte1, byte2, byte3;
        byte1 = byte2 = byte3 = 0x0;

        //add length bits.
        byte1 |= (byte)((Length & 0x18) << 3);
        byte3 |= (byte)((Length & 0x7) << 5);

        //now the address bits.
        byte[] bytes = BitConverter.GetBytes(SnesAddress);
        byte2 = bytes[0];
        byte1 |= (byte)(((bytes[2] - 0x20) << 2) & 0x3C);

        byte1 |= (byte)((bytes[1] & 0x60) >> 5);
        byte3 |= (byte)(bytes[1] & 0x1F);

        byte[] bytes__ = new byte[] { byte1, byte2, byte3 };
        GetAddress_MetalSlader_SNES(bytes__, out ulong PC_Address_, out int Length_);

        if (Global.Verbose)
        {
            Console.WriteLine($"Address: 0x{PC_Address:X5}. Length #${Length:X2}");
        }

        //Sanity check.
        if (PC_Address_ != PC_Address || Length_ != Length)
        {
            if (Global.Verbose)
            {
                Console.WriteLine($"Address: 0x{PC_Address_:X5}. Length #${Length_:X2}. Bad conversion.");
            }
            throw new Exception("Conversion unsuccessful. Aborting.");
        }

        return BitConverter.ToUInt32(new byte[] { byte3, byte2, byte1, 0 }, 0);
    }

    /*
 * Address layout (in bits)
 * ------------------------
 * 
 * a, b     = length bytes
 * x, y, z  = Address bytes
 *       
 * a1 a2 x1 x2 x3 x4 x5 x6      y1 y2 y3 y4 y5 y6 y7 y8     b1 b2 b3 z1 z2 z3 z4 z5
 * 
 * length   = - - - a1 a2 b1 b2 b3
 * address  =         
 *      
 *      Original:
 *                          x1 x2 x3 
 *      cccccccc =  0  0  0  0  0 x1 x2 x3 
 *      dddddddd = x4 x5 x6 z1 z2 z3 z4 z5
 *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
 *      
 *      Final address = cccccccc dddddddd eeeeeeee
 */

    public ulong CreateAddress_MetalSlader_NES(ulong PC_Address, int Length, bool IsTranslation = false)
    {
        ulong SnesAddress = PC_Address;
        byte byte1, byte2, byte3;
        byte1 = byte2 = byte3 = 0x0;
        
        byte1 |= (byte)((Length & 0x18) << 3);
        byte3 |= (byte)((Length & 0x7) << 5); //add length bits.

        //now the address bits.
        byte[] bytes = BitConverter.GetBytes(SnesAddress);
        byte2 = bytes[0];

        byte1 |= (byte)((bytes[2] << 3) & 0x78);
        byte1 |= (byte)((bytes[1] & 0xE0) >> 5);
        byte3 |= (byte)((bytes[1] & 0x1F));

        GetAddress_MetalSlader_NES(new byte[] { byte1, byte2, byte3 }, out ulong PC_Address_, out int Length_, IsTranslation);

        if (Global.Verbose)
        {
            Console.WriteLine($"Address: 0x{PC_Address:X5}. Length #${Length:X2}");
        }

        //Sanity check.
        if (PC_Address_ != PC_Address || (Length_ != Length ? (Length != 0) : false))
        {
            if (Global.Verbose)
            {
                Console.WriteLine($"Address: 0x{PC_Address_:X5}. Length #${Length_:X2}. Bad conversion.");
            }
            throw new Exception("Conversion unsuccessful. Aborting.");
        }

        return BitConverter.ToUInt32(new byte[] { byte3, byte2, byte1, 0 }, 0);
    }

    public void GetAddress_MetalSlader_SNES(byte[] b, out ulong PointerAddress, out int Length)
    {
        /*
         * Address layout (in bits)
         * ------------------------
         * 
         * a, b     = length bytes
         * x, y, z  = Address bytes
         *       
         * a1 a2 x1 x2 x3 x4 x5 x6      y1 y2 y3 y4 y5 y6 y7 y8     b1 b2 b3 z1 z2 z3 z4 z5
         * 
         * length   = - - - a1 a2 b1 b2 b3
         * address  =         
         *                                   x            
         *      cccccccc =  0  0  1  0 x1 x2 x3 x4
         *      dddddddd =  1 x5 x6 z1 z2 z3 z4 z5
         *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
         *      
         *      Final address = cccccccc dddddddd eeeeeeee
         *                      
         *   Smallest address = 00100000 10000000 00000000 => 0x104000
         *   Biggest address  = 00101111 11111111 11111111 => 0x2FFFFF
         *   							 ^
	     *   The bit with the arrows under it must always be 1. So not all addresses
         *   can be pointed to between 0x208000 and 0x2FFFFF. 
         *   The pointers cover a wide range, so it makes you wonder why the programmer
         *   even bothered with dictionary compression. It may be an artifact
         *   from the NES port.
         */

        if (b.Length != 3)
        {
            throw new Exception("Pointer must be a length of 3.");
        }

        // 24-bit UNPACKER
        int byte1 = b[0] & 0x3f;
        int byte2 = b[2] & 0x1f;
        int byte3 = b[1];

        byte2 |= ((byte1 & 0x03) << 5) + 0x80;
        byte1 = 0x20 + ((byte1 & 0xfc) >> 2);

        ulong ptr = (ulong)((byte1 << 16) + (byte2 << 8) + (byte3 << 0));

        //Pointer that points to dialogue.
        PointerAddress = ptr;

        Length = (b[0] & 0xc0) >> 3;
        Length |= (b[2] & 0xe0) >> 5;

        if (Global.Verbose)
        {
            Console.WriteLine($"Address: 0x{PointerAddress:X5}. Length #${Length:X2}");
        }
    }

    public void GetAddress_MetalSlader_NES(byte[] b, out ulong PointerAddress, out int Length, bool isRomExpanded = false)
    {
        /*
         * Address layout (in bits)
         * ------------------------
         * 
         * a, b     = length bytes
         * x, y, z  = Address bytes
         *       
         * a1 * x1 x2 x3 x4 x5 x6      y1 y2 y3 y4 y5 y6 y7 y8     b1 b2 b3 z1 z2 z3 z4 z5
         * 
         *   * = Used a length bit for dictionary. Dialogue entries never use the length bit,
         *   therefore, the translation uses this to use the expanded PRG.
         *   So there asterisk can represent a2 and x0.
         * 
         * 
         * 
         * length   = - - - a1 a2 b1 b2 b3
         * address  =         
         *      
         *      Original:
         * 
         *      cccccccc =  0  0  0  0  0 x1 x2 x3 
         *      dddddddd = x4 x5 x6 z1 z2 z3 z4 z5
         *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
         *      
         *      Translation:
         * 
         *      cccccccc =  0  0  0 a1 a2 x1 x2 x3 
         *      dddddddd = x4 x5 x6 z1 z2 z3 z4 z5
         *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
         *      
         *      
         *      Final address = cccccccc dddddddd eeeeeeee
         */

        if (b.Length == 3)
        {
            // 24-bit UNPACKER

            int byte1 = b[0] & 0x3F;

            if (isRomExpanded)
            {
                byte1 = b[0] & 0x7F;
            }

            int byte2 = b[2] & 0x1f;
            int byte3 = b[1];

            byte2 |= ((byte1 & 0x07) << 5);
            //byte2 |= ((byte1 & 0x04) << 6);

            if (!isRomExpanded)
            {
                byte1 = ((byte1 & 0x3c) >> 3);
            }
            else
            {
                byte1 = ((byte1 & 0x7C) >> 3);
            }

            ulong ptr = (ulong)((byte1 << 16) + (byte2 << 8) + (byte3 << 0));

            //Pointer that points to dialogue.
            PointerAddress = ptr;

            Length = (b[0] & 0xc0) >> 3;
            Length |= (b[2] & 0xe0) >> 5;
        }
        else throw new Exception("Pointer must be a length of 3.");
    }

    public void GetAddress_CustomFormat(byte[] b, string[] Format, out ulong PointerAddress)
    {
        byte[] PointerArray = new byte[8];

        for(int i = 0; i < Format.Length; i++)
        {
            string Val = Format[i];

            if (Val.Contains('('))
            {
                Val = Val.Replace("(", "").Replace(")", "");
                int j = int.Parse(Val[1].ToString());
                PointerArray[j] = b[i];
            }
            else
            {
                if (Val != "XX" && b[i] != MyMath.hexToDec(Val))
                {
                    throw new Exception($"Custom format mismatch. ROM: 0x{b[i]:X2}  Pointer format: 0x{Val:X2}.");
                }
            }
        }

        PointerAddress = BitConverter.ToUInt64(PointerArray, 0);
    }

    public byte[] CreateAddress_CustomFormat(ulong PC_Address, string[] Format)
    {
        byte[] PointerAsBytes = BitConverter.GetBytes(PC_Address);

        byte[] NewPointer = new byte[Format.Length];

        for (int i = 0; i < Format.Length; i++)
        {
            string Val = Format[i];

            if (Val.Contains('('))
            {
                Val = Val.Replace("(", "").Replace(")", "");
                
                //get X value in BX
                int j = int.Parse(Val[1].ToString());

                //because we reversed the order, we d
                NewPointer[i] = PointerAsBytes[j];
            }
            else
            {
                NewPointer[i] = (byte)MyMath.hexToDec(Val);
            }
        }
        return NewPointer;
    }

    public void GetAddress_Arle_Adventure_GBC_7_Byte_Pointer(byte[] b, out ulong PointerAddress)
    {
        if (b[1] != 0x22 && b[2] != 0x3E && b[4] != 0x22 && b[5] != 0x3E)
        {
            throw new Exception("Wrong format.");
        }

        PointerAddress = BitConverter.ToUInt32(new byte[] { b[0], b[3], b[6], 0 }, 0);
    }

    public byte[] CreateAddress_Arle_Adventure_GBC_7_Byte_Pointer(ulong PC_Address)
    {
        byte[] bytes = BitConverter.GetBytes(PC_Address);

        if (BitConverter.IsLittleEndian)
        {
            Array.Reverse(bytes);
        }

        return new byte[] { bytes[3], 0x22, 0x3E, bytes[2], 0x22, 0x3E, bytes[1] };
    }


    public void GetDictionaryPointer(int EntryNo, ulong AddressOfPointer, out ulong PointerAddress, int SizeOfEachEntry)
    {
        PointerAddress = AddressOfPointer + (ulong)EntryNo * (ulong)SizeOfEachEntry;
    }

    public void GetPointer_NoPointer_NoDelimeter(ulong CurrentAddress, byte[][] Delimeter, out ulong NextAddress)
    {
        NextAddress = CurrentAddress;
        for (ulong rom_i = CurrentAddress; rom_i < (ulong)ROM.DATA.Length; rom_i++)
        {
            for(int delim_i = 0; delim_i < Delimeter.Length; delim_i++)
            {
                if (ROM.DATA[rom_i] == Delimeter[delim_i][0])
                {
                    bool Matches = true;
                    ulong i = rom_i + 1;
                    for (; i < (ulong)Delimeter.Length; i++)
                    {
                        if (ROM.DATA[i] == Delimeter[delim_i][i - rom_i])
                        {
                            Matches = true;
                        }
                        else
                        {
                            Matches = false;
                            break;
                        }
                    }

                    if (Matches)
                    {
                        NextAddress = i;
                        goto Getout;
                    }
                }
            }
        }
        throw new Exception("Delimeter not found.");
    Getout:;
    }

    public void ReadLittleEndian(byte[] b, out ulong PointerAddress)
    {
        switch (b.Length)
        {
            case 1:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[0] }, 0);
                break;
            case 2:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[0], b[1], 0, 0 }, 0);
                break;
            case 3:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[0], b[1], b[2], 0 }, 0);
                break;
            case 4:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[0], b[1], b[2], b[3] }, 0);
                break;
            default:
                throw new Exception();
        }
    }

    public void ReadBigEndian(byte[] b, out ulong PointerAddress)
    {
        switch (b.Length)
        {
            case 1:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[0] }, 0);
                break;
            case 2:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[1], b[0], 0, 0 }, 0);
                break;
            case 3:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[2], b[1], b[0], 0 }, 0);
                break;
            case 4:
                PointerAddress = BitConverter.ToUInt32(new byte[] { b[3], b[2], b[1], b[0] }, 0);
                break;
            default:
                throw new Exception();
        }
    }

    public byte[] GetLittleEndian(ulong Address, int Length)
    {
        byte[] Converted = BitConverter.GetBytes(Address);
        byte[] Done = new byte[Length];

        Array.Copy(Converted, 0, Done, 0, Length);

        return Done;
    }
}
