﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

/// <summary>
/// Read the CYRPO file, which contains most 
/// configuration information on how to
/// dump and insert text.
/// </summary>
public static class Settings
{
    public static bool BlankOutTextDataAfterRead;
    public static byte BlankOutByte;
    public static bool MirrorBlankDictionaryEntries;

    public static string ROMToCopy { set; get; }
    public static string ROMToWrite { set; get; }
    public static string ROM_Original { set; get; }
    public static string ROM_BlankOut { set; get; }


    private static Pointer_Metadata[] Pointer_Read_info_;
    private static Pointer_Metadata[] Pointer_Write_info_;


    public static Pointer_Metadata[] Pointer_Read_info { get => Pointer_Read_info_; }
    public static Pointer_Metadata[] Pointer_Write_info { get => Pointer_Write_info_; }

    public static List<Pointer_Write> Script_insertion_info { get; private set; }
    public static WriteableRange[] WriteableRange { get; private set; }

    public static void ReadSettingsFile(string PathOfSettingsFile)
    {
        if (Global.Verbose)
        {
            Console.WriteLine("Reading settings file.");
        }

        Pointer_Read_info_ = new Pointer_Metadata[0];
        Pointer_Write_info_ = new Pointer_Metadata[0];

        if (!File.Exists(PathOfSettingsFile))
        {
            throw new FileNotFoundException($"Cannot find the following file: {PathOfSettingsFile}");
        }

        string Project_Dir = Path.GetDirectoryName(PathOfSettingsFile);

        int GetInt(XElement ele, string TagName)
        {
            if (ele.Element(TagName) == null)
            {
                throw new Exception($"Tag missing: \"{TagName}\".");
            }

            return int.Parse(ele.Element(TagName).Value);
        }
        int GetIntFromHex(XElement ele, string TagName)
        {
            if (ele.Element(TagName) == null)
            {
                throw new Exception($"Tag missing: \"{TagName}\".");
            }

            int retme = -1;
            string InnerText = ele.Element(TagName).Value;

            if (InnerText.Contains("0x"))
            {
                retme = MyMath.hexToDec(InnerText);
            }
            else
            {
                retme = int.Parse(InnerText);
            }

            return retme;
        }

        XElement x = XDocument.Load(PathOfSettingsFile).Element(Constants.ROOT);

        ROM_Original = string.Format(x.Element("Options").Element("Original").Value, Project_Dir);
        ROM_BlankOut = string.Format(x.Element("Options").Element("Blanked").Value, Project_Dir);
        BlankOutTextDataAfterRead = x.Element("Options").Element("BlankOutTextDataAfterRead").Value == "Y";
        var Raw_byte_blank = x.Element("Options").Element("BlankOutByte").Value;

        //use the key in dictionary file as dictionary value
        //only useful when the game doesn't use a dictionary
        //The dictionary file is a mandatory file.

        var temp = x.Element("Options").Element("MirrorBlankDictionaryEntries");

        if (temp != null)
        {
            MirrorBlankDictionaryEntries = temp.Value == "Y";
        }
        else
        {
            MirrorBlankDictionaryEntries = false;
        }

        if (Raw_byte_blank != null)
        {
            BlankOutByte = (byte)MyMath.hexToDec(Raw_byte_blank);
        }

        foreach (XElement InnerTags in x.Element("Pointer").Elements())
        {
            if (InnerTags.Name == "e")
            {
                ReadPointerInstructions(InnerTags, ref Pointer_Read_info_);
                Pointer_Write_info_ = Pointer_Read_info_;
            }
            else
            {
                if (InnerTags.Name == "Read")
                {
                    foreach (XElement a in InnerTags.Elements())
                    {
                        ReadPointerInstructions(a, ref Pointer_Read_info_);
                    }
                }
                else if (InnerTags.Name == "Write")
                {
                    foreach (XElement a in InnerTags.Elements())
                    {
                        ReadPointerInstructions(a, ref Pointer_Write_info_, false);
                    }
                }
            }

            void ReadPointerInstructions(XElement a, ref Pointer_Metadata[] PointerInfo, bool IsReadInfo = true)
            {
                List<Pointer_Metadata> l_PointerInfo = new List<Pointer_Metadata>();

                if (a.Element("PcDifference") != null)
                {

                    string pcDiff_raw = a.Element("PcDifference").Value;
                    bool PCDifference_Adds = true;

                    int PCDiff = 0;

                    if (pcDiff_raw != "")
                    {
                        PCDiff = (int)ProcessAddOrSub(pcDiff_raw);
                        PCDifference_Adds = PCDiff > 0;
                        PCDiff = Math.Abs(PCDiff);
                    }

                    int GetAttributeNumber(string ElementName, string AttributeName)
                    {
                        int Value = -1;

                        var raw_delim = a.Element(ElementName).Attribute(AttributeName);

                        if (raw_delim != null)
                        {
                            if (!((string)raw_delim).Contains("0x"))
                            {
                                Value = int.Parse(raw_delim.Value);
                            }
                            else
                            {
                                Value = MyMath.hexToDec(raw_delim.Value);
                            }
                        }

                        return Value;
                    }


                    var Delim_Obj = a.Element("DialogueReadType").Attribute("Delim");

                    byte[][] delimeter;

                    if (Delim_Obj != null)
                    {
                        string[] Split_delim = ((string)Delim_Obj).Split('|');
                        delimeter = new byte[Split_delim.Length][];
                        int j = 0;
                        foreach (string delimentry in Split_delim)
                        {
                            string[] s_Delims = delimentry.Replace("0x", "").Split(',');
                            delimeter[j] = new byte[s_Delims.Length];

                            for (int i = 0; i < s_Delims.Length; i++)
                            {
                                delimeter[j][i] = (byte)MyMath.hexToDec(s_Delims[i]);
                            }
                            j++;
                        }
                    }
                    else
                    {
                        delimeter = new byte[0][];
                    }

                    int FixedSize = GetAttributeNumber("PrimaryPointerType", "FixedSize");
                    int DictionaryInteractionType = GetAttributeNumber("RenderType", "DictionaryInteractionType");


                    foreach (string Addr in a.Element("Address").Value.Split(Constants.SPLIT_ADDRESS))
                    {
                        string CHRFile = a.Element("CHRFile").Value;
                        string DictTemplate = a.Element("DictionaryTemplateFile").Value;

                        var squishytext_file = a.Element("SquishyTextFile");


                        var cust_format = a.Element("Address").Attribute("CustomFormat");

                        string[] s_cust = new string[] { };

                        if (cust_format != null)
                        {
                            s_cust = cust_format.Value.Split(',');
                        }

                        var XML_name = a.Element("Name");

                        string SquishyTextFile = "";

                        bool SquishyTextFile_useregex = false;


                        if (squishytext_file != null)
                        {
                            SquishyTextFile = squishytext_file.Value;
                            var useregex = squishytext_file.Attribute("UseRegex");

                            if (useregex != null)
                            {
                                SquishyTextFile_useregex = useregex.Value == "Y";
                            }
                        }

                        string Name = "";

                        if (XML_name != null)
                        {
                            Name = XML_name.Value;
                        }

                        bool StoreOutOfOrderToSaveSpace = false;

                        XElement x_StoreOutOfOrderToSaveSpace = a.Element("StoreOutOfOrderToSaveSpace");

                        if (x_StoreOutOfOrderToSaveSpace != null)
                        {
                            StoreOutOfOrderToSaveSpace = x_StoreOutOfOrderToSaveSpace.Value == "Y";
                        }

                        bool ZeroIgnore = false;
                        XElement XZeroIgnore = a.Element("IgnoreNegativeAndZeroPointers");
                        if (XZeroIgnore != null)
                        {
                            ZeroIgnore = XZeroIgnore.Value == "Y";
                        }

                        int BytesBetween = 0;
                        if (a.Element("BytesBetween") != null)
                        {
                            BytesBetween = GetIntFromHex(a, "BytesBetween");
                        }

                        l_PointerInfo.Add
                        (
                            new Pointer_Metadata
                            (
                                Name: Name,
                                Address_of_pointer: (ulong)ProcessAddOrSub(Addr),
                                PCDifference: PCDiff,
                                PCDifference_Adds: PCDifference_Adds,
                                EntryNumber: GetIntFromHex(a, "EntryNumber"),
                                DialogueReadType: GetInt(a, "DialogueReadType"),
                                HasSecondaryPointer: a.Element("HasSecondaryPointer").Value == "Y",
                                PrimaryPointerType: GetInt(a, "PrimaryPointerType"),
                                FixedSize: FixedSize,
                                PrimaryPointerLength: GetInt(a, "PrimaryPointerLength"),
                                SecondaryPointerType: GetInt(a, "SecondaryPointerType"),
                                SecondaryPointerLength: GetInt(a, "SecondaryPointerLength"),
                                Delimeter: delimeter,
                                RenderType: GetInt(a, "RenderType"),
                                DictionaryInteractionType: DictionaryInteractionType,
                                IsDictionary: a.Element("IsDictionary").Value == "Y",
                                AddressConversion: GetInt(a, "AddressConversion"),
                                CHRFile: a.Element("CHRFile").Value,
                                DictionaryTemplateFile: DictTemplate,
                                SquishyTextFile: SquishyTextFile,
                                SquishyTextFile_useregex: SquishyTextFile_useregex,
                                PixelsPerLine: int.Parse(a.Element("PixelsPerLine").Value),
                                InserLineBreaks: a.Element("InserLineBreaks").Value == "Y",
                                AutoLineBreak: int.Parse(a.Element("AutoLineBreak").Value),
                                StoreOutOfOrderToSaveSpace: StoreOutOfOrderToSaveSpace,
                                SpecialPointerFormat: s_cust,
                                IgnoreNegativeAndZeroPointers: ZeroIgnore,
                                BytesBetween: BytesBetween
                             )
                        );
                    }
                }
                else
                {
                    if (!IsReadInfo)
                    {
                        /*
                         * Grab read entry info 
                         * if write entry info is empty.
                         */
                        l_PointerInfo.Add(Pointer_Read_info_[PointerInfo.Length]);
                    }
                    else
                    {
                        throw new Exception("Read entry cannot be emoty.");
                    }
                }
                PointerInfo = PointerInfo.Concat(l_PointerInfo).ToArray();
            }
        }

        if (Pointer_Read_info_.Length != Pointer_Write_info_.Length)
        {
            throw new Exception($"Read and write entry count not the same. Read: {Pointer_Read_info_.Length}   Write: {Pointer_Write_info_.Length}:");
        }

        Script_insertion_info = new List<Pointer_Write>();

        x = x.Element("Write");

        ROMToCopy = string.Format(x.Element("ROMToCopy").Value, Project_Dir);
        ROMToWrite = string.Format(x.Element("ROMToWrite").Value, Project_Dir);

        foreach (XElement WriteRegion in x.Element("WriteRegionCollection").Elements())
        {
            List<int> EntryOwner = new List<int>();
            List<int> WriteableId = new List<int>();

            foreach (XElement e in WriteRegion.Element("EntryOwners").Elements())
            {
                foreach (string s in e.Value.ToString().Split(','))
                {
                    EntryOwner.Add(int.Parse(s));
                }
            }

            foreach (XElement Range in WriteRegion.Element("WriteableAddressRanges").Elements())
            {
                foreach (string s in Range.Value.ToString().Split(','))
                {
                    WriteableId.Add(int.Parse(s));
                }
            }

            Script_insertion_info.Add(new Pointer_Write(EntryOwner.ToArray(), WriteableId.ToArray()));
        }

        List<WriteableRange> w = new List<WriteableRange>();
        foreach (XElement WriteRegion in x.Element("WriteableRangeCollection").Elements())
        {
            string Name = "";
            XElement xx = WriteRegion.Element("Name");
            if (xx != null)
            {
                Name = xx.Value;
            }

            w.Add(new WriteableRange(Name, (ulong)ProcessAddOrSub(WriteRegion.Element("StartAddress").Value), MyMath.hexToDec(WriteRegion.Element("Size").Value)));
        }

        WriteableRange = w.ToArray();
    }

    private static long ProcessAddOrSub(string rawString)
    {
        if (rawString.Contains("+") || rawString.Contains("-"))
        {
            string[] a_chunks = (new Regex(@"(\+)|(\-)")).Split(rawString).Where(s => s != String.Empty).ToArray();

            long Address = 0;
            bool IsAdd = true;
            foreach (string s in a_chunks)
            {

                switch (s)
                {
                    case "+":
                        IsAdd = true;
                        break;
                    case "-":
                        IsAdd = false;
                        break;
                    default:
                        long Multipler = IsAdd ? 1 : -1;
                        Address += Multipler * (long)MyMath.hexToUInt64(s);
                        break;
                }
            }
            return Address;
        }
        else
        {
            return (long)MyMath.hexToUInt64(rawString);
        }
    }
}


public struct Pointer_Write
{
    public int[] EntryOwner;
    public int[] WriteableId;

    public Pointer_Write(int[] WriteRegion, int[] WriteableRegion)
    {
        this.EntryOwner = WriteRegion;
        this.WriteableId = WriteableRegion;
    }
}

public struct WriteableRange
{
    public string Name;
    public ulong StartAddress;
    public int Size;

    public WriteableRange(string Name, ulong StartAddress, int Size)
    {
        this.Name = Name;
        this.StartAddress = StartAddress;
        this.Size = Size;
    }
}

public struct Pointer_Metadata
{
    public string Name { get; private set; }

    public ulong Address_of_pointer { get; private set; }
    public int PCDifference { get; private set; }
    public bool PCDifference_Adds { get; private set; }
    public int EntryNumber { get; private set; }
    public int DialogueReadType { get; private set; }
    public bool HasSecondaryPointer { get; private set; }
    public int PrimaryPointerType { get; private set; }
    public int FixedSize { get; private set; }
    public int PrimaryPointerLength { get; private set; }
    public int SecondaryPointerType { get; private set; }
    public int SecondaryPointerLength { get; private set; }
    public byte[][] Delimeter { get; private set; }
    public int RenderType { get; private set; }
    public int DictionaryInteractionType { get; private set; }
    public bool IsDictionary { get; private set; }
    public int AddressConversion { get; private set; }
    public string CHRFile { get; private set; }
    public string DictionaryTemplateFile { get; private set; }
    public string SquishyTextFile { get; private set; }
    public bool SquishyTextFile_useregex { get; private set; }
    public int PixelsPerLine { get; private set; }
    public bool InserLineBreaks { get; private set; }
    public int AutoLineBreak { get; private set; }
    public bool StoreOutOfOrderToSaveSpace { get; private set; }
    public string[] SpecialPointerFormat { get; private set; }
    public bool IgnoreNegativeAndZeroPointers { get; private set; }
    public int BytesBetween { get; private set; }

    public Pointer_Metadata
    (
        string Name,
        ulong Address_of_pointer,
        int PCDifference,
        bool PCDifference_Adds,
        int EntryNumber,
        int DialogueReadType,
        bool HasSecondaryPointer,
        int PrimaryPointerType,
        int FixedSize,
        int PrimaryPointerLength,
        int SecondaryPointerType,
        int SecondaryPointerLength,
        byte[][] Delimeter,
        int RenderType,
        int DictionaryInteractionType,
        bool IsDictionary,
        int AddressConversion,
        string CHRFile,
        string DictionaryTemplateFile,
        string SquishyTextFile,
        bool SquishyTextFile_useregex,
        int PixelsPerLine,
        bool InserLineBreaks,
        int AutoLineBreak,
        bool StoreOutOfOrderToSaveSpace,
        string[] SpecialPointerFormat,
        bool IgnoreNegativeAndZeroPointers,
        int BytesBetween
        )
    {
        this.Name = Name;
        this.Address_of_pointer = Address_of_pointer;
        this.PCDifference = PCDifference;
        this.PCDifference_Adds = PCDifference_Adds;
        this.EntryNumber = EntryNumber;
        this.DialogueReadType = DialogueReadType;
        this.HasSecondaryPointer = HasSecondaryPointer;
        this.PrimaryPointerType = PrimaryPointerType;
        this.FixedSize = FixedSize;
        this.PrimaryPointerLength = PrimaryPointerLength;
        this.SecondaryPointerType = SecondaryPointerType;
        this.SecondaryPointerLength = SecondaryPointerLength;
        this.Delimeter = Delimeter;
        this.RenderType = RenderType;
        this.DictionaryInteractionType = DictionaryInteractionType;
        this.IsDictionary = IsDictionary;
        this.AddressConversion = AddressConversion;
        this.CHRFile = CHRFile;
        this.DictionaryTemplateFile = DictionaryTemplateFile;
        this.SquishyTextFile = SquishyTextFile;
        this.SquishyTextFile_useregex = SquishyTextFile_useregex;
        this.PixelsPerLine = PixelsPerLine;
        this.InserLineBreaks = InserLineBreaks;
        this.AutoLineBreak = AutoLineBreak;
        this.StoreOutOfOrderToSaveSpace = StoreOutOfOrderToSaveSpace;
        this.SpecialPointerFormat = SpecialPointerFormat;
        this.IgnoreNegativeAndZeroPointers = IgnoreNegativeAndZeroPointers;
        this.BytesBetween = BytesBetween;
    }
}