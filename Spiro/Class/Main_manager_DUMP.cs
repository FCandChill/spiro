﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using static Enums;

partial class Main_manager
{
    public void DumpScript(bool CreateAceFiles)
    {
        if (!File.Exists(Settings.ROM_Original))
        {
            throw new FileNotFoundException($"File not found: {Settings.ROM_Original}");
        }

        //read ROM
        byte[] RomBinaryData = File.ReadAllBytes(Settings.ROM_Original);
        ROM.SetRom(RomBinaryData, out bool isHeadered);

        // MonologueIO mono1 = new MonologueIO(true);
        //string line = mono1.BytesToString__Table(ROM.Read(0x015D7A8, 11).ToList());

        //read settings file.
        Settings.ReadSettingsFile(cypro_path);

        PointerManager Ptr_reader = new PointerManager();
        //int aa = Ptr_reader.CreateAddress_MetalSlader(0x177C88, 0x008);

        //Ptr_reader.GetAddress_MetalSlader(new byte[] { 0x2A, 0xFF, 0x1F }, out int Address11, out int Length11);

        //Read pointer table and store to array.
        void ReadPrimaryPointer()
        {
            for (int IndexTable = 0; IndexTable < Settings.Pointer_Read_info.Length; IndexTable++)
            {
                ulong Min = ulong.MaxValue, Max = ulong.MinValue;
                List<ulong> pointer_adrress;
                List<int> Length;

                pointer_adrress = new List<ulong>();
                Length = new List<int>();

                ulong Type_10_CurrentAddress = Settings.Pointer_Read_info[IndexTable].Address_of_pointer;
                ulong Type_10_NextAddress = Settings.Pointer_Read_info[IndexTable].Address_of_pointer;

                bool ProcessedPrimaryPointer = false;
                for (int EntryNo = 0; EntryNo < Settings.Pointer_Read_info[IndexTable].EntryNumber; EntryNo++)
                {
                    ulong Created_Pointer_;
                    int Created_Length_;
                    bool GoingToBreak_;

                    if (Settings.Pointer_Read_info[IndexTable].HasSecondaryPointer)
                    {
                        if (!ProcessedPrimaryPointer)
                        {
                            ReadPointer(Settings.Pointer_Read_info[IndexTable].PrimaryPointerType, Settings.Pointer_Read_info[IndexTable].PrimaryPointerLength, out Created_Pointer_, out Created_Length_, out GoingToBreak_);
                            ProcessedPrimaryPointer = true;
                            Type_10_CurrentAddress = Type_10_NextAddress = Created_Pointer_;
                            EntryNo--;
                        }
                        else
                        {
                            ReadPointer(Settings.Pointer_Read_info[IndexTable].SecondaryPointerType, Settings.Pointer_Read_info[IndexTable].SecondaryPointerLength, out Created_Pointer_, out Created_Length_, out GoingToBreak_, true); ;
                            ProcessedPrimaryPointer = true;

                            Length.Add(Created_Length_);
                            pointer_adrress.Add(Created_Pointer_);

                            if (GoingToBreak_)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        ReadPointer(Settings.Pointer_Read_info[IndexTable].PrimaryPointerType, Settings.Pointer_Read_info[IndexTable].PrimaryPointerLength, out Created_Pointer_, out Created_Length_, out GoingToBreak_);

                        Length.Add(Created_Length_);
                        pointer_adrress.Add(Created_Pointer_);

                        if (GoingToBreak_)
                        {
                            break;
                        }
                    }

                    void ReadPointer(int PointerType, int PointerLength, out ulong Created_Pointer, out int Created_Length, out bool GoingToBreak, bool DisablePCDifference = false)
                    {
                        ulong PointerAddress;
                        int Length1;
                        ulong AddressOfPointer = Settings.Pointer_Read_info[IndexTable].Address_of_pointer;
                        ulong buffer_ptr = AddressOfPointer + (ulong)(EntryNo * PointerLength) + (ulong)(EntryNo * Settings.Pointer_Read_info[IndexTable].BytesBetween);
                        byte[] PointerBytes = new byte[PointerLength];

                        if (PointerBytes.Length != 0)
                        {
                            Array.Copy(ROM.DATA, (int)buffer_ptr, PointerBytes, 0, PointerBytes.Length);
                        }

                        Length1 = Settings.Pointer_Read_info[IndexTable].FixedSize;

                        if (Global.Verbose)
                        {
                            Console.Write($"Reading address: 0x{buffer_ptr:X2}; Name: {Settings.Pointer_Read_info[IndexTable].Name};Index: {EntryNo:0000};");
                        }

                        bool NoPointer = false;
                        switch (PointerType)
                        {
                            case (int)Enums.PointerType.None:
                                PointerAddress = buffer_ptr;
                                break;
                            case (int)Enums.PointerType.LittleEndian:
                                Ptr_reader.ReadLittleEndian(PointerBytes, out PointerAddress);
                                break;
                            case (int)Enums.PointerType.Three_Byte_Metal_Slader_DX_Primary_NoLength:
                            case (int)Enums.PointerType.Three_Byte_Metal_Slader_DX_Primary:
                                Ptr_reader.GetAddress_MetalSlader_SNES(PointerBytes, out PointerAddress, out Length1);
                                break;
                            case (int)Enums.PointerType.Three_Byte_MetalSlader_NES_Primary_NoLength:
                            case (int)Enums.PointerType.Three_Byte_MetalSlader_NES_Primary:
                                Ptr_reader.GetAddress_MetalSlader_NES(PointerBytes, out PointerAddress, out Length1);
                                break;
                            case (int)Enums.PointerType.Three_Byte_MetalSlader_NES_trans_Primary_NoLength:
                                Ptr_reader.GetAddress_MetalSlader_NES(PointerBytes, out PointerAddress, out Length1, true);
                                break;
                            case (int)Enums.PointerType.BigEndian:
                                Ptr_reader.ReadBigEndian(PointerBytes, out PointerAddress);
                                break;
                            case (int)Enums.PointerType.Arle_Adventure_GBC_7_Byte_Pointer:
                                Ptr_reader.GetAddress_Arle_Adventure_GBC_7_Byte_Pointer(PointerBytes, out PointerAddress);
                                break;
                            case (int)Enums.PointerType.NoDelimeterNoPointer_Fixed:
                                NoPointer = true;
                                Ptr_reader.GetDictionaryPointer(EntryNo, AddressOfPointer, out PointerAddress, Length1);
                                break;
                            case (int)Enums.PointerType.NoPointer_Delimeter:
                                NoPointer = true;
                                PointerAddress = AddressOfPointer;
                                break;
                            case (int)Enums.PointerType.Custom:
                                Ptr_reader.GetAddress_CustomFormat(PointerBytes, Settings.Pointer_Read_info[IndexTable].SpecialPointerFormat, out PointerAddress);
                                break;
                            default:
                                throw new Exception($"Invalid pointertype number: {PointerType}");
                        }

                        if (!NoPointer)
                        {
                            switch (Settings.Pointer_Read_info[IndexTable].AddressConversion)
                            {
                                case (int)AddressConversion.None:
                                    break;
                                case (int)Enums.AddressConversion.LOROM:
                                    PointerAddress = (ulong)AddressLoROM.snesToPc((int)PointerAddress);
                                    break;
                                case (int)Enums.AddressConversion.HiROM:
                                    PointerAddress = (ulong)AddressHiRom.snesToPc((int)PointerAddress);
                                    break;
                                default:
                                    throw new Exception();
                            }

                            if (!DisablePCDifference)
                            {
                                int PCDifference = Settings.Pointer_Read_info[IndexTable].PCDifference;

                                if (!Settings.Pointer_Read_info[IndexTable].PCDifference_Adds)
                                {
                                    PCDifference *= -1;
                                }

                                PointerAddress = (ulong)(PCDifference + (int)PointerAddress);
                            }
                        }

                        if (PointerAddress < 0)
                        {
                            throw new Exception($"Pointer less than zero. Location 0x{AddressOfPointer:X2}.");
                        }
                        else if ((int)PointerAddress > ROM.DATA.Length)
                        {
                            throw new Exception($"Pointer bigger than ROM size. Location 0x{AddressOfPointer:X2}.");
                        }

                        string str = "";
                        foreach (byte bty in PointerBytes)
                        {
                            str = string.Concat(str, bty.ToString("X2"));
                        }

                        if (Global.Verbose)
                        {
                            Console.WriteLine($"Bytes: {str}; Pointer Address: 0x{PointerAddress:X8}; Length: 0x{Length1:X3}");
                        }

                        Created_Length = Length1;
                        Created_Pointer = PointerAddress;

                        Min = Math.Min(Min, PointerAddress);
                        Max = Math.Max(Max, PointerAddress);

                        /*
                         * This loop is for reading a pointer table. If there's no pointer table,
                         * then get out of here.
                         */
                        if (PointerType == (int)Enums.PointerType.None)
                        {
                            GoingToBreak = true;
                        }
                        else
                        {
                            GoingToBreak = false;
                        }
                    }
                }

                Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable] = pointer_adrress.ToArray();
                Ptr_reader.Master_Length[IndexTable] = Length.ToArray();

                if (Global.Verbose)
                {
                    //Console.WriteLine(string.Format("Smallest address: 0x{0}, Biggest address: 0x{1}", Min.ToString("X2"), Max.ToString("X2")));
                }
            }
        }

        ReadPrimaryPointer();

        MonologueIO mono = null;

        File.Delete(Original);
        File.Delete(Byte_dump);

        XDocument xdoc_original = new XDocument();
        XDocument xdoc_comment = new XDocument();
        XDocument xdoc_byteDump = new XDocument();

        XElement root_original = new XElement(Constants.ROOT);
        XElement root_comment = new XElement(Constants.ROOT);
        XElement root_byteDump = new XElement(Constants.ROOT);

        DictionaryTextfileIO dictio = null;

        for (int IndexTable = 0; IndexTable < Settings.Pointer_Read_info.Length; IndexTable++)
        {
            //Get Filename of CHR files and Dictionary files.
            string CHRFile = Settings.Pointer_Read_info[IndexTable].CHRFile;
            string DictionaryTemplateFile = Settings.Pointer_Read_info[IndexTable].DictionaryTemplateFile;

            if (!string.IsNullOrEmpty(CHRFile))
            {
                Global.Path_To_CHR_orig = string.Format(Constants.Path_To_x_orig, CHRFile);
            }

            if (!string.IsNullOrEmpty(DictionaryTemplateFile))
            {
                //check if file is already open.
                if (Global.Path_To_Dict_orig != string.Format(Constants.Path_To_x_orig, DictionaryTemplateFile))
                {
                    Global.Path_To_Dict_orig = string.Format(Constants.Path_To_x_orig, DictionaryTemplateFile);
                    Global.Path_To_Dict_Template = string.Format(Constants.Path_To_dict_template, DictionaryTemplateFile);
                    dictio = new DictionaryTextfileIO(Global.Path_To_Dict_orig);

                    mono = new MonologueIO(true, true);
                }
            }

            ulong[] PointerArray;
            PointerArray = Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable];


            List<byte> Script;
            List<byte> Script_withoutDelimester;

            XAttribute Name = new XAttribute("Name", Settings.Pointer_Read_info[IndexTable].Name);

            XElement sc_en_original = new XElement(Constants.SCRIPT_ENTRY, new XAttribute(Constants.NUMBER, IndexTable), Name);
            XElement sc_en_comment = new XElement(Constants.SCRIPT_ENTRY, new XAttribute(Constants.NUMBER, IndexTable), Name);
            XElement sc_en_byte_dump = new XElement(Constants.SCRIPT_ENTRY, new XAttribute(Constants.NUMBER, IndexTable), Name);

            for (int indexNumber = 0; indexNumber < PointerArray.Length; indexNumber++)
            {
                switch (Settings.Pointer_Read_info[IndexTable].DialogueReadType)
                {
                    case (int)Enums.DialogueReadType.HasDelimeter:
                        Script_withoutDelimester = new List<byte>();
                        Script = mono.ReadByteStream_with_pointer_and_delim(PointerArray, IndexTable, indexNumber, Settings.Pointer_Read_info[IndexTable].Delimeter, Settings.Pointer_Read_info[IndexTable].DictionaryInteractionType, ref Script_withoutDelimester);

                        switch ((Settings.Pointer_Read_info[IndexTable].PrimaryPointerType))
                        {
                            case (int)Enums.PointerType.NoPointer_Delimeter:
                                if (indexNumber + 1< Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable].Length)
                                {
                                    Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable][indexNumber + 1] = Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable][indexNumber] + (ulong)Script.Count;
                                }
                                break;

                        }

                        break;

                    case (int)Enums.DialogueReadType.HasLengthVar:
                        Script = mono.ReadByteStream_with_pointer_in_size(PointerArray, Ptr_reader.Master_Length[IndexTable], indexNumber);
                        Script_withoutDelimester = Script;
                        break;
                    default:
                        throw new Exception();
                }

                if (Global.Verbose)
                {
                    Console.Out.WriteLine($"Index table{IndexTable}; Index:{indexNumber:0000}; Primary: 0x" + Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable][indexNumber].ToString("X2") + (Ptr_reader.Master_Secondary_Pointer_adrresses[IndexTable] != null ? "; Secondary:  " + Ptr_reader.Master_Secondary_Pointer_adrresses[IndexTable][indexNumber].ToString("X2") : "") + ":");
                }

                string bytes = "";
                foreach (byte b in Script)
                {
                    bytes = string.Concat(bytes, Constants.BYTE1 + b.ToString("X2") + Constants.BYTE2);
                }

                if (Global.Verbose)
                {
                    Console.Out.WriteLine(bytes);
                }

                /*
                 *Load dictionary if it's the first entry. Modify text file.
                 */
                if (Settings.Pointer_Read_info[IndexTable].IsDictionary)
                {
                    dictio.WriteEntry(Script_withoutDelimester);
                }

                string Line = null;

                switch (Settings.Pointer_Read_info[IndexTable].RenderType)
                {
                    //Byte => text with CHR table.
                    case (int)RenderType.NoDictionary_BigEndian:
                        Line = mono.BytesToString_NoDictionary(Script, false);
                        break;
                    //Byte => text with dictionary table.
                    case (int)RenderType.Dictionary:
                        Line = mono.BytesToString_Dictionary_Table(Script, Settings.Pointer_Read_info[IndexTable].DictionaryInteractionType);
                        break;
                    case (int)RenderType.NoDictionary_MetalSladerNameCard:
                        Line = mono.BytesToString_MetalSladerNameCards(Script);
                        break;
                    //Byte => text with CHR table.
                    case (int)RenderType.NoDictionary_LittleEndian:
                        Line = mono.BytesToString_NoDictionary(Script, true);
                        break;
                    default:
                        throw new Exception();
                }



                if (Settings.Pointer_Read_info[IndexTable].SquishyTextFile != string.Empty)
                {
                    SquishyText squish = new SquishyText(string.Format(Constants.Path_To_x_orig, Settings.Pointer_Read_info[IndexTable].SquishyTextFile), Settings.Pointer_Write_info[IndexTable].SquishyTextFile_useregex);
                    squish.ReadSquishyTextFile();
                    Line = squish.AddSquishyText(Line);
                }


                ulong SecondaryPointer = (Ptr_reader.Master_Secondary_Pointer_adrresses[IndexTable] != null ? Ptr_reader.Master_Secondary_Pointer_adrresses[IndexTable][indexNumber] : 0);

                XAttribute Primary = new XAttribute("PrimaryAddress", $"0x{MyMath.decToHex(Ptr_reader.Master_Primary_Pointer_adrresses[IndexTable][indexNumber])}");
                XAttribute EntryNo = new XAttribute(Constants.NUMBER, indexNumber);

                sc_en_original.Add(new XElement("e", Line, EntryNo, Primary, new XAttribute("Byte_Length", $"0x{MyMath.decToHex(Script.Count)}")));
                sc_en_comment.Add(new XElement("e", "", EntryNo, Primary));
                sc_en_byte_dump.Add(new XElement("e", bytes, EntryNo));
            }

            if (Settings.Pointer_Read_info[IndexTable].IsDictionary)
            {
                /* 
                 * Write the dictionary data to the file if 
                 * 
                 * 1) we are switching tables to read
                 * 2) if the next IndexTable is not a dictionary
                 * 3) If the dictionary is the last IndexTable.
                 * 
                 */
                dictio.Write();
                mono = new MonologueIO(true, true);
            }

            root_original.Add(sc_en_original);
            root_comment.Add(sc_en_comment);
            root_byteDump.Add(sc_en_byte_dump);
        }

        xdoc_original.Add(root_original);
        xdoc_comment.Add(root_comment);
        xdoc_byteDump.Add(root_byteDump);

        xdoc_original.Declaration = new XDeclaration("1.0", "UTF-8", null);
        xdoc_comment.Declaration = new XDeclaration("1.0", "UTF-8", null);
        xdoc_byteDump.Declaration = new XDeclaration("1.0", "UTF-8", null);

        xdoc_original.Save(Original);

        if (CreateAceFiles)
        {
            xdoc_comment.Save(New, SaveOptions.None);       //the New,
            xdoc_comment.Save(Comment, SaveOptions.None);   //menu,
            xdoc_comment.Save(Menu, SaveOptions.None);      //and Comment xml are identical.
            xdoc_byteDump.Save(Byte_dump, SaveOptions.None);
        }

        if (Settings.BlankOutTextDataAfterRead)
        {
            File.WriteAllBytes(Settings.ROM_BlankOut, ROM.DATA);
        }
    }
}