﻿using System;

class AddressLoROM
{
    /// <summary>
    /// Address conversions
    /// </summary>
    /// <param name="addr"></param>
    /// <param name="hasHeader"></param>
    /// <returns></returns>
    public static int pcToSnes(int addr, bool hasHeader = false)
    {
        //lazy code
        int lo = pcToSnes_Lo(addr, hasHeader),
            hi = pcToSnes_Hi(addr, hasHeader),
            bank = pcToSnes_Bank(addr, hasHeader);
        return MyMath.hexToDec(MyMath.decToHex(bank) + "" + MyMath.decToHex(hi) + "" + MyMath.decToHex(lo));
    }
    public static int snesToPc(int address, bool hasHeader = false)
    {
        const int temp1 = 0x100, temp2 = temp1 * temp1, temp3 = temp2 * temp1;
        //I don't know exactly why this works, but it does.
        return snesToPc(
            address % temp1,            //lo
            address % temp2 / temp1,    //hi
            address % temp3 / temp2,    //bank
            hasHeader);
    }

    //source: https://www.smwcentral.net/?p=viewthread&t=13167&page=1&pid=188431#p188431
    public static int snesToPc(int lo, int hi, int bank, bool hasHeader = false)
    {
        if (((bank & 0xF0) == 0x70) /*|| ((hi & 0x80) != 0x80)*/)
        {
            throw new Exception("Invalid parameters for SNES ROM.");
        }
        return (lo & 0xFF) + (0x100 * (hi & 0xFF)) + (0x8000 * (bank & 0x7F)) - header_f(!hasHeader) - 0x7E00;
    }
    //source: https://www.smwcentral.net/?p=viewthread&t=13167&page=1&pid=188431#p188431
    public static int pcToSnes_Lo(int addr, bool hasHeader = false)
    {
        validCheck(addr, hasHeader);
        return (addr & 0xFF);
    }
    //source: https://www.smwcentral.net/?p=viewthread&t=13167&page=1&pid=188431#p188431
    public static int pcToSnes_Hi(int addr, bool hasHeader = false)
    {
        validCheck(addr, hasHeader);
        return (((addr - header_f(hasHeader)) / 0x100) & 0x7F) + 0x80;
    }
    //source: https://www.smwcentral.net/?p=viewthread&t=13167&page=1&pid=188431#p188431
    public static int pcToSnes_Bank(int addr, bool hasHeader = false)
    {
        validCheck(addr, hasHeader);
        int returnVal = ((addr - header_f(hasHeader)) / 0x8000);
        if (addr >= (0x380000 + (header_f(hasHeader))))
        {
            returnVal = returnVal | 0x80;
        }
        return returnVal;
    }
    private static void validCheck(int PC_Address, bool hasHeader)
    {
        if ((hasHeader && (PC_Address < 0x200)) || (PC_Address >= 0x400000 + header_f(hasHeader)))
        {
            throw new Exception("Invalid parameters for SNES ROM.");
        }
    }

    private static int header_f(bool hasHeader) => (hasHeader) ? 0x200 : 0;
}