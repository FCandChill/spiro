﻿/* 
 * Class        :   CharacterTable.cs
 * Author       :   FCandChill
 * Description  :   
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
/// <summary>
/// A class that stores and manages the dictionary for
/// the monologues. It also makes interpretting
/// and compressing faster.
/// </summary>

public class MyDictionary
{
    public SortedDictionary<byte[], byte[]> Dict_Sort_By_Key { get; set; }
    public SortedDictionary<byte[], KeyValuePair<string,int>> Dict_Controlcode { get; set; }
    private SortedDictionary<byte[], string> CHR_by_key;
    private SortedDictionary<string, byte[]> CHR_by_string;

    public SortedDictionary<string, List<KeyValuePair<byte[], byte[]>>> Dict_Sort_By_Character_With_Byte_Val { get; set; }
    public SortedDictionary<string, List<KeyValuePair<byte[], string>>> Dict_Sort_By_Character_With_String_Val { get; set; }
    public SortedDictionary<string, string> Dict_Controlcode_Id{ get; set; }

    public SortedDictionary<string, int> Dict_Sort_By_Character_With_String_Val_MaxLength { get; set; }

    public bool DictionaryContainsKey(byte[] Key) => Dict_Sort_By_Key.ContainsKey(Key);

    private const int characterTableSize = byte.MaxValue;

    public bool IsCHRValid(byte[] b) => CHR_by_key.ContainsKey(b);

    public string GetCHRValue(byte[] b)
    {
        if (!CHR_by_key.ContainsKey(b))
        {
            //throw new Exception(string.Format("Value #${0} isn't in the CHR file. Add it in the TBL file.", MyMath.decToHex(b)));

            return Constants.BYTE1 + MyMath.decToHex(b) + Constants.BYTE2;
        }
        return CHR_by_key[b];
    }

    public string GetCHRValue(byte[] b, out bool Found)
    {
        Found = true;
        if (!CHR_by_key.ContainsKey(b))
        {
            Found = false;
            return Constants.BYTE1 + MyMath.decToHex(b) + Constants.BYTE2;
        }
        return CHR_by_key[b];
    }

    public bool GetCHRByteValue(string Entry, out byte[] Key)
    {
        bool pass;
        Key = new byte[0];
        if (pass = CHR_by_string.ContainsKey(Entry))
        {
            Key = CHR_by_string[Entry];
        }
        return pass;
    }

    public string GetDictionaryKeyAsString_DictionaryReferencesCHRExceptForControlCodes(byte[] Key, out int CodeArgumentLength, bool isLittleEndian = false)
    {
        CodeArgumentLength = Constants.CODE_ARGUMENT_LENGTH_NULL;

        string s = "";
        byte b1, b2, b3, b4;
        byte[] CHR_1, CHR_2, CHR_3, CHR_4;

        if (Dict_Controlcode.ContainsKey(Key))
        {
            s = Dict_Controlcode[Key].Key;
            CodeArgumentLength = Dict_Controlcode[Key].Value;
        }
        else
        {
            byte[] Byte_data = Dict_Sort_By_Key[Key];

            byte LookAhead(int index, out bool IsOutOfBounds)
            {
                IsOutOfBounds = false;
                if (index < Byte_data.Length)
                {
                    return Byte_data[index];
                }
                else
                {
                    IsOutOfBounds = true;
                    return 0;
                }
            }

            for (int i = 0; i < Byte_data.Length;)
            {
                b1 = LookAhead(i + 0, out bool OutOfBounds1);
                b2 = LookAhead(i + 1, out bool OutOfBounds2);
                b3 = LookAhead(i + 2, out bool OutOfBounds3);
                b4 = LookAhead(i + 3, out bool OutOfBounds4);


                if (isLittleEndian)
                {
                    CHR_1 = new byte[] { b1 };
                    CHR_2 = new byte[] { b2, b1 };
                    CHR_3 = new byte[] { b3, b2, b1 };
                    CHR_4 = new byte[] { b4, b3, b2, b1 };
                } 
                else
                {
                    CHR_1 = new byte[] { b1 };
                    CHR_2 = new byte[] { b1, b2 };
                    CHR_3 = new byte[] { b1, b2, b3 };
                    CHR_4 = new byte[] { b1, b2, b3, b4 };
                }


                if (CHR_by_key.ContainsKey(CHR_4) && !OutOfBounds4 && CHR_4 != CHR_3)
                {
                    s += CHR_by_key[CHR_4];
                    i += 4;
                }
                else if (CHR_by_key.ContainsKey(CHR_3) && !OutOfBounds3 && CHR_3 != CHR_2)
                {
                    s += CHR_by_key[CHR_3];
                    i += 3;
                }
                else if (CHR_by_key.ContainsKey(CHR_2) && !OutOfBounds2 && CHR_2 != CHR_1)
                {
                    s += CHR_by_key[CHR_2];
                    i += 2;
                }
                else if (CHR_by_key.ContainsKey(CHR_1) && !OutOfBounds1)
                {
                    s += CHR_by_key[CHR_1];
                    i += 1;
                }
                else
                {
                    s += Constants.BYTE1 + MyMath.decToHex(CHR_1) + Constants.BYTE2;
                    i += 1;
                }
            }
        }

        return s;
    }


    public struct Byte_Struct
    {
        public byte[] IntegerVal;
        public bool LookaheadOutOfBounds;

        public Byte_Struct(byte[] IntegerVal, bool LookaheadOutOfBounds)
        {
            this.IntegerVal = IntegerVal;
            this.LookaheadOutOfBounds = LookaheadOutOfBounds;
        }
    }


    public string GetDictionaryKeyAsString_DictionaryReferencesDictionary(byte[] Key, out int CodeArgumentLength)
    {
        CodeArgumentLength = Constants.CODE_ARGUMENT_LENGTH_NULL;

        string s = "";
        byte b1, b2, b3, b4;

        if (Dict_Controlcode.ContainsKey(Key))
        {
            s = Dict_Controlcode[Key].Key;
            CodeArgumentLength = Dict_Controlcode[Key].Value;
        }
        else
        {
            byte[] Byte_data = Dict_Sort_By_Key[Key];

            byte LookAhead(int index, out bool IsOutOfBounds)
            {
                IsOutOfBounds = false;
                if (index < Byte_data.Length)
                {
                    return Byte_data[index];
                }
                else
                {
                    IsOutOfBounds = true;
                    return 0;
                }
            }

            for (int i = 0; i < Byte_data.Length;)
            {
                b1 = LookAhead(i + 0, out bool OutOfBounds1);
                b2 = LookAhead(i + 1, out bool OutOfBounds2);
                b3 = LookAhead(i + 2, out bool OutOfBounds3);
                b4 = LookAhead(i + 3, out bool OutOfBounds4);

                Byte_Struct[] l = new Byte_Struct[4];

                for (int k = 0; k < l.Length; k++)
                {
                    byte[] Value;
                    bool OutOfBounds;
                    switch (k)
                    {
                        case 0: Value = new byte[] { b1 }; OutOfBounds = OutOfBounds1; break;
                        case 1: Value = new byte[] { b1, b2,  }; OutOfBounds = OutOfBounds2; break;
                        case 2: Value = new byte[] { b1, b2, b3 }; OutOfBounds = OutOfBounds3; break;
                        case 3: Value = new byte[] { b1, b2, b3, b4 }; OutOfBounds = OutOfBounds4; break;
                        default: throw new Exception();
                    }
                    l[k] = new Byte_Struct(Value, OutOfBounds);
                }


                for (int k = l.Length - 1; k >= -1; k--)
                {
                    bool IsControlCode = false;

                    if (k < 0)
                    {
                        s += Constants.BYTE1 + MyMath.decToHex(l[k + 1].IntegerVal) + Constants.BYTE2;
                        i += 1;
                        break;
                    }

                    if ((IsControlCode = Dict_Controlcode.ContainsKey(l[k].IntegerVal)) || Dict_Sort_By_Key.ContainsKey(l[k].IntegerVal))
                    {
                        if (IsControlCode)
                        {
                            s += Dict_Controlcode[l[k].IntegerVal].Key;
                        }
                        else
                        {
                            s += GetDictionaryAsString(Dict_Sort_By_Key[l[k].IntegerVal], out _);
                        }
                        i += k + 1;
                        break;
                    }
                }
            }
        }

        return s;
    }

    public string PrintEntireDictionary()
    {
        string s = "";
        foreach (KeyValuePair<byte[], byte[]> kvp in Dict_Sort_By_Key)
        {
            s += $"0x{MyMath.decToHex(kvp.Key)}: {GetDictionaryAsString(Dict_Sort_By_Key[kvp.Key], out _)}\n";
        }

        return s;
    }


    /// <summary>
    /// Initilializes a new instance of CharacterTable
    /// </summary>
    public MyDictionary(bool HaveDicrionary, bool ReadOriginal)
    {
        if (ReadOriginal)
        {
            if (Global.Path_To_CHR_orig == null)
            {
                throw new Exception();
            }

            Read_CHR_File(Global.MainFolder, Global.Path_To_CHR_orig, out CHR_by_key, out CHR_by_string);
        }
        else
        {
            if (Global.Path_To_CHR_trans == null)
            {
                throw new Exception();
            }

            Read_CHR_File(Global.MainFolder, Global.Path_To_CHR_trans, out CHR_by_key, out CHR_by_string);
        }

        if (HaveDicrionary)
        {
            Regex regex1 = new Regex(Constants.REGEX_SPLIT_STRING);


            //read dictionary
            Dict_Controlcode = new SortedDictionary<byte[], KeyValuePair<string, int>>(new ByteComparer());

            if (ReadOriginal)
            {
                Dict_Sort_By_Key = Read_DictionaryFile(Global.Path_To_Dict_orig);
            }
            else
            {
                Dict_Sort_By_Key = Read_DictionaryFile(Global.Path_To_Dict_trans);
            }

            Dict_Sort_By_Character_With_String_Val = new SortedDictionary<string, List<KeyValuePair<byte[], string>>>();
            Dict_Sort_By_Character_With_Byte_Val = new SortedDictionary<string, List<KeyValuePair<byte[], byte[]>>>();
            Dict_Sort_By_Character_With_String_Val_MaxLength = new SortedDictionary<string, int>();
            Dict_Controlcode_Id = new SortedDictionary<string, string>();

            int IterationNumber = 0;

            foreach (KeyValuePair<byte[], byte[]> kvp in Dict_Sort_By_Key)
            {
                /*
                * The Table_Sort_By_Character variables are for writing to the ROM. 
                * It's best to structure the dictionary in a digestible format to make the 
                * writing quicker. 
                * 
                * When writing to the ROM, we have to detect dictionary keywords within a range.
                * The range is as big as the biggest dictionary entry. In other words,
                * there's no need to check for a word that's 10 characters long
                * that starts with 'a' when the biggest entry is 4 characters long.
                * That's why we also store a structure to store the biggest length 
                * for a character.
                * 
                * Also, the Action keywords are a replacement for dictionary, so we treat it as a value to search for.
                */

                string FirstCharacter = "";
                string FirstCharacter_Special = "";


                string Dictionary_Value = "";
                string Dictionary_Value_Special = "";

                bool ActionCharacter = false;

                Dictionary_Value = GetDictionaryAsString(kvp.Value, out FirstCharacter);

                //ignore inserting blank entries.
                if (kvp.Value.Length != 0)
                {
                    //If part of the in-game dictionary (and not action keyword)
                    //get value of first byte

                    //FirstCharacter = GetCHRValue(kvp.Value[0]);

                    if (Dict_Controlcode.ContainsKey(kvp.Key))
                    {
                        //If special character, get value from another datastructure
                        //The first character will be the first char in the string.

                        Dictionary_Value_Special = Dict_Controlcode[kvp.Key].Key;
                        FirstCharacter_Special = Dictionary_Value_Special.ToString();
                        ActionCharacter = true;
                    }

                    /*
                     * We store the byte data of the action keywords
                     * in the dictionary table HOWEVER we don't want to 
                     * compress it using that data. Therefore,
                     * we don't store the byte data in the data structures.
                     */

                    if (ActionCharacter)
                    {
                        //Add the linked list entry if it doesn't exist. Otherwise, we will be trying
                        //to insert into a null field, that will crash the program.
                        if (!Dict_Sort_By_Character_With_Byte_Val.ContainsKey(FirstCharacter_Special))
                        {
                            //Add to datastructures
                            Dict_Sort_By_Character_With_Byte_Val.Add(FirstCharacter_Special, new List<KeyValuePair<byte[], byte[]>>());
                            Dict_Sort_By_Character_With_String_Val.Add(FirstCharacter_Special, new List<KeyValuePair<byte[], string>>());
                            Dict_Sort_By_Character_With_String_Val_MaxLength.Add(FirstCharacter_Special, 0);
                        }


                        //Insert the special action keyword as well.
                        //The program writes dictionaries and scripts, so I have to
                        //support both.

                        //byte data, sorted by first character

                        string[] a_SubChunk = regex1.Split(Dictionary_Value_Special).Where(s => s != string.Empty).ToArray();

                        Dict_Sort_By_Character_With_String_Val_MaxLength[FirstCharacter_Special] = Math.Max(Dict_Sort_By_Character_With_String_Val_MaxLength[FirstCharacter_Special], a_SubChunk.Length);

                        //string value, sorted by first character
                        Dict_Sort_By_Character_With_Byte_Val[FirstCharacter_Special].Add(kvp);

                        //string value, sorted by first character
                        Dict_Sort_By_Character_With_String_Val[FirstCharacter_Special].Add(new KeyValuePair<byte[], string>(kvp.Key, Dictionary_Value_Special));

                    }
                    else
                    {
                        //Add the linked list entry if it doesn't exist. Otherwise, we will be trying
                        //to insert into a null field, that will crash the program.
                        if (!Dict_Sort_By_Character_With_Byte_Val.ContainsKey(FirstCharacter))
                        {
                            Dict_Sort_By_Character_With_Byte_Val.Add(FirstCharacter, new List<KeyValuePair<byte[], byte[]>>());
                            Dict_Sort_By_Character_With_String_Val.Add(FirstCharacter, new List<KeyValuePair<byte[], string>>());
                            Dict_Sort_By_Character_With_String_Val_MaxLength.Add(FirstCharacter, 0);
                        }

                        Dict_Sort_By_Character_With_String_Val_MaxLength[FirstCharacter] = Math.Max(Dict_Sort_By_Character_With_String_Val_MaxLength[FirstCharacter], Dictionary_Value.Length);
                        Dict_Sort_By_Character_With_Byte_Val[FirstCharacter].Add(kvp);
                        Dict_Sort_By_Character_With_String_Val[FirstCharacter].Add(new KeyValuePair<byte[], string>(kvp.Key, Dictionary_Value));
                    }
                }
                IterationNumber++;
            }
        }
    }

    private string GetDictionaryAsString(byte[] b, out string FirstCharacter)
    {
        FirstCharacter = "";
        bool First = true;
        string Decoded = "";
        byte[] Master_cut;
        int i = 0;
        while (i < b.Length)
        {
            int Length = 4;

            if (Length + i > b.Length)
            {
                Length = b.Length - i;
            }

            bool Found = false;
            int j = Length;
            byte[] PossibleId = new byte[0];
            string tempresult = "";
            while (j > 0)
            {
                if (b[i] != 0x00)
                {
                    Master_cut = new byte[j];

                    Array.Copy(b, i, Master_cut, 0, j);

                    List<byte> aa = Master_cut.ToList();

                    PossibleId = aa.ToArray();
                }
                else
                {
                    PossibleId = new byte[] { 0x0 };
                    j = 1;
                }
                tempresult = GetCHRValue(PossibleId, out Found);
                j--;
                if (Found)
                {
                    break;
                }
            }

            if (First)
            {
                FirstCharacter = tempresult;
                First = false;
            }
            Decoded = string.Concat(Decoded, tempresult);
            i += j + 1;
        }
        return Decoded;
    }

    /// <summary>
    /// The the table file (says what each CHR tile is)
    /// </summary>
    /// <param name="main"></param>
    /// <param name="FileName"></param>
    /// <param name="chr_by_key"></param>
    /// <param name="chr_by_value"></param>
    private void Read_CHR_File(string main, string FileName, out SortedDictionary<byte[], string> chr_by_key, out SortedDictionary<string, byte[]> chr_by_value)
    {
        if (Global.Verbose)
        {
            Console.WriteLine("Reading CHR file.");
        }

        string DictPath = Path.Combine(main, FileName);
        if (!File.Exists(DictPath))
        {
            throw new FileNotFoundException($"The table file doesn't exist in the path: {DictPath}.");
        }

        chr_by_key = new SortedDictionary<byte[], string>(new ByteComparer());
        chr_by_value = new SortedDictionary<string, byte[]>();

        using (FileStream inputOpenedFile = File.Open(DictPath, FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8))
            {
                foreach (string fileLine in sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (Global.Verbose)
                    {
                        Console.WriteLine(fileLine);
                    }

                    byte[] HexVal = GetHexValueFromFileLine(fileLine);
                    string StringVal = GetstringFromFileLine(fileLine);
                    if (StringVal != string.Empty)
                    {
                        if (!chr_by_key.ContainsKey(HexVal))
                        {
                            if (
                                StringVal.Contains(Constants.BYTE1.ToString()) ||
                                StringVal.Contains(Constants.BYTE2.ToString()) ||
                                StringVal.Contains(Constants.ACTION1.ToString()) ||
                                StringVal.Contains(Constants.ACTION2.ToString())
                                )
                            {
                                throw new Exception($"You cannot use special characters in the CHR file. You used the following special character for the value 0x{HexVal:X2}: {StringVal}");

                            }

                            chr_by_key[HexVal] = StringVal;
                            chr_by_value[StringVal] = HexVal;
                        }
                        else throw new Exception($"Duplicate key {HexVal}.");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Changes encodingt to UTF7.
    /// 
    /// //https://stackoverflow.com/questions/14057434/how-can-i-transform-string-to-utf-8-in-c
    /// </summary>
    /// <param name="utf8String"></param>
    /// <returns></returns>
    private static string ProperEncoding(string utf8String)
    {
        byte[] utf8_Bytes = new byte[utf8String.Length];
        for (int i = 0; i < utf8String.Length; ++i)
        {
            utf8_Bytes[i] = (byte)utf8String[i];
        }

        return Encoding.UTF8.GetString(utf8_Bytes, 0, utf8_Bytes.Length);
    }

    private SortedDictionary<byte[], byte[]> Read_DictionaryFile(string Filename)
    {
        Filename = Path.Combine(Global.MainFolder, Filename);
        if (!File.Exists(Filename))
        {
            throw new FileNotFoundException($"The table file doesn't exist in the path: {Filename}");
        }

        SortedDictionary<byte[], byte[]> table = new SortedDictionary<byte[], byte[]>(new ByteComparer());

        using (StreamReader sr = new StreamReader(Filename, Encoding.UTF8, true))
        {
            string[] AllLines = sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string fileLine in AllLines)
            {
                /*
                 * Ignore blank lines.
                 */
                if (fileLine.Contains(Constants.DICTIONARY_SPLIT))
                {
                    if (!table.ContainsKey(GetHexValueFromFileLine(fileLine)))
                    {
                        string Line = GetstringFromFileLine(fileLine);

                        byte[] KeyValue = GetHexValueFromFileLine(fileLine);

                        if (Line.Contains(Constants.ACTION1.ToString()) && Line.Contains(Constants.ACTION2.ToString()))
                        {
                            int Action1_lastIndex = Line.LastIndexOf(Constants.ACTION1);

                            string Action = Line.Substring(Action1_lastIndex, Line.LastIndexOf(Constants.ACTION2) - Action1_lastIndex + 1);

                            int codeLength;

                            if (fileLine.Contains(Constants.CODE_LENGTH1.ToString()))
                            {
                                codeLength = int.Parse(fileLine.Split(Constants.CODE_LENGTH1, Constants.CODE_LENGTH2)[1]);
                            }
                            else codeLength = Constants.CODE_ARGUMENT_LENGTH_NULL;

                            //extract byte values only.
                            Line = Regex.Replace(Line, @"(\(.*?\))|(\<.*?\>)|\{|\}", "");

                            Dict_Controlcode.Add(KeyValue, new KeyValuePair<string, int>(Action, codeLength));
                            table.Add(KeyValue, MyMath.hexToBytes(Line));
                        }

                        else if (Line.Contains(Constants.BYTE1.ToString()))
                        {
                            Line = Line.Replace(Constants.BYTE1.ToString(), "").Replace(Constants.BYTE2.ToString(), "");
                            table.Add(KeyValue, MyMath.hexToBytes(Line));
                        }

                        //when the bytes aren't provided,
                        //check settings to see if we can use the key as the bytes.
                        else if (Settings.MirrorBlankDictionaryEntries)
                        {
                            if (KeyValue.Length == 0)
                            {
                                /*
                                 * We trim the zeros off above. But we add
                                 * a zero if it the value is zero. Otherwise,
                                 * the array is empty.
                                 */
                                KeyValue = new byte[] { 0x0 };
                            }

                            table.Add(KeyValue, KeyValue);
                        }
                    }
                    else
                    {
                        throw new Exception("Duplicate key found: " + fileLine);
                    }
                }
            }
        }
        return table;
    }

    /// <summary>
    /// Get the byte value (the first part, before the equal sign).
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private static byte[] GetHexValueFromFileLine(string str)
    {
        string ProperlyEncoded = ProperEncoding(str);
        int index = ProperlyEncoded.IndexOf(Constants.SEPERATOR);
        return MyMath.hexToBytes(str.Substring(0, index));
    }
    private static string GetstringFromFileLine(string str)
    {
        string ProperlyEncoded = ProperEncoding(str);
        int index = ProperlyEncoded.IndexOf(Constants.SEPERATOR);
        return str.Substring(index + Constants.SEPERATOR.Length).ToString();
    }
}
