﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class DictionaryTextfileIO
{
    private StreamReader sr;
    private string AllFile = "";
    private string NewFilePath;
    private List<List<byte>> MyDictionary;
    private static List<string> l_filename = new List<string>();

    public DictionaryTextfileIO(string FileName)
    {
        if (string.IsNullOrEmpty(FileName))
        {
            throw new Exception();
        }

        NewFilePath = Path.Combine(Global.MainFolder + FileName);
        string OldTBLFile = Path.Combine(Global.MainFolder + Global.Path_To_Dict_Template);

        if (!File.Exists(OldTBLFile))
        {
            throw new FileNotFoundException($"Can't find the file: {OldTBLFile}. ");
        }

        //don't delete files if we've created them in this session.
        if (!l_filename.Contains(FileName))
        {
            //Don't delete dictionary if readonly
            //We can delete this, but it's a nice feature to have.
            FileInfo fi = new FileInfo(Path.Combine(Global.MainFolder + FileName));
            if (!fi.IsReadOnly)
            {
                File.Delete(Path.Combine(Global.MainFolder + FileName));
                File.Copy(OldTBLFile, NewFilePath);
            }
            l_filename.Add(FileName);
        }

        MyDictionary = new List<List<byte>>();
    }

    public void WriteEntry(List<byte> l_byte)
    {
        MyDictionary.Add(l_byte);
    }

    public void Write()
    {
        sr = new StreamReader(NewFilePath);

        string[] Entries_To_Write = new string[MyDictionary.Count];
        string s;
        int i = 0;
        foreach (List<byte> l_b in MyDictionary)
        {
            s = "";
            foreach (byte b in l_b)
            {
                s = string.Concat(s, Constants.BYTE1 + MyMath.decToHex(b) + Constants.BYTE2);
            }

            Entries_To_Write[i++] = s;
        }
        i = 0;
        AllFile = "";
        string Entry = "";

        while (!sr.EndOfStream)
        {
            string Line = sr.ReadLine();
            if (Line.Contains(Constants.DICTIONARY_SPLIT) && !Line.Contains(Constants.BYTE1))
            {
                if (i < Entries_To_Write.Length)
                {
                    Entry = Entries_To_Write[i];
                }
                else
                {
                    Entry = "";
                }
            }
            else
            {
                Entry = "";
            }

            if (Line.Contains(Constants.DICTIONARY_SPLIT))
            {
                i++;
            }

            AllFile = string.Concat(AllFile, Line + Entry + Environment.NewLine);
        }

        sr.Close();
        File.WriteAllText(NewFilePath, AllFile);
    }
}
