# Cypro Ace and Spiro
Cypro Ace and Spiro are tools used for translating video games. Each have their own purpose:

* Spiro is a commandline script dumper and writer. It reads a project folder for its information and dumps and reads the script in an XML format.
* Cypro Ace is a GUI script manager that lets you view the original and translated scripts side by side. It also lets you export and import excel files.

## Preface

The first time I looked at cartographer and atlas, 
I thought there was a difficulty curve, the script dumps were hard to manage as being in flatfiles, and the source code was complex and hard to follow.
So I made my own suite. It's by no means perfect, but it helped translation projects. For the following reasons:

* The script inserter of spiro.exe automatically inserted line breaks.
* The scripter viewer showed a preview of what the text would look like in-game.
* The script dumping and insertion was faily quick because of the usage of keyvalue pairs to handle dictionaries.
* I could insert the script and luanch the emualtor with two mouse button clicks inside Cypro Ace.
* I was given more control compared to atlas as to where dialogoue was written to. I could make a pointer table point to multiple regions and even store them out of order to save space.

However, I can also see people not liking the utilities:

* It's not [Atlas](https://www.romhacking.net/utilities/224/) and [Cartographer](https://www.romhacking.net/utilities/647/).
* XML, ew.
* No multifile support
* [adcde](https://www.romhacking.net/utilities/1392/) has more features.
* Bad embedded pointer support
    * Easily one of my most biggest regrets with the utility. Because:
		* Embedded pointers are supported when writing, which is something akin to Atlas. Currently, there's no support for dumping these pointers.
* Bad multi-pointer support
    * The assumption at the time of implementation is that a pointer would point to another pointer which would point to dialogue (secondary pointers). This is a pretty bad assumption and should probably be scrapped. A better implentation would be to suport an unlimited amount.
## Second version

I have a few regrets with this application. Some ideas for a version 2.0

* Do a complete overhaul of the `Main.cypro`.
	* Make it closer to Atlas and Cartographer where you select different options in a flat file format.
		* It would have more optional selections.
		* This would allow me to overhaul how embedded pointers are handled.
	* Have a GUI instead of managing different options in XML.
* Change everything over to JSON. It would probably speed up the searching in Cypro Ace.
* Maybe make it a web application.

## Spiro
The script dumper is a command line tool.

### How to use the command line

Parameters in brackets are optional.

The pipe, `|`, means you can choose either or.

```
sprio.exe
    /PathToCyproFolder $PATH
    /WriteScriptToROM | /ReadScript
    [/CreateAceFiles]
    [/Verbose]
```

* WriteScriptToROM
    * Write the script to the ROM file. File name is from your XML cypro file.
* DumpScript
    * Dump script to XML file.
* CreateAceFiles
    * Create Comment, Menu, and New XML files when dumping script. It will overwrite the files if they exist. Be careful!
    * Also creates a byte dump file.
* Verbose
    * Verbose output. In other words, it provides more information when running the program.
* Example command
    * `Spiro.exe /PathToCyproFolder "C:\Cypro Ace Tales of Phantasia" /WriteScriptToROM /Verbose`
    * `Spiro.exe /PathToCyproFolder "C:\Cypro Ace Tales of Phantasia" /DumpScript /Verbose`

### BAT file

You can run the program without opening the command line.

1. Add your command to a .txt file.
2. Change the extension to a .bat
3. You can now click the bat file to run the command.

## Cypro Ace

![](https://bitbucket.org/FCandChill/spiro/raw/57c4cc31be9027cfbe4b31245c2c07a4c0bad4ab/Cypro_screenshot.png)

[Cypro Ace](https://bitbucket.org/FCandChill/cypro-ace/src/master/) is a Windows Form Application. It allows you to manage the dumped script and other XML files.

## Overview of dumping and writing process

Before using spiro, you’ll need to set up the cypro file, if someone hasn’t set it up for you. You’ll need to understand how the dumping and writing process works in sprio chronologically.

### Read pointers
* Load pointer bytes and process them
    * Games store their pointers differently. For example, Metal Slader Glory stores the length of the dialogue in the pointer. Most games will either store no pointer data or store a pointer in little endian.
* Address Conversion
    * Apply LoROM or HiROM conversion if needed.
* Apply PC difference
    * Add or subtract from processed address. Useful for FDS games or pointers that are bank specific.
* Dialogue processing
    * Dialogue Read
        * Load bytes of dialogue to memory. With or without delimiter.
    * Render
        * Convert bytes to text.
### Writing process
* Encode
    * Load every script entry and encode, based on the RenderType tag in cypro’s XML tag. Add the delimiter byte(s) if needed.
* Writing
    * The process of writing to the ROM is complex.
        * In the XML, you can set up several data regions to store the dialogue. You specify what pointer tables can point to which region.
* Dictionary
    * During both of the above processes, a dictionary file is automatically generated. If a region is marked as a dictionary, its text entries will be loaded to the a dictionary file. The generated dictionary file will be used when dumping or inserting the script. The dictionary file is based on a dictionary template.

## Overview Project folder 
The Cypro Ace and Spiro translation package depend on a project folder. It contains all the data for your translation.

* `Main.cypro`
    * This file is an XML file that either you or someone else has generated for you. See below for a brief overview of what it contains:
* `CHR.tbl` & `CHR - trans.tbl`
    * The CHR table file for the original and translation ROM respectively.
* `Dictionary_template.tbl` & `Dictionary.tbl` & `Dictionary – trans.tbl`
    * Set up the template file if you are reading the dictionary from the ROM.
    * Setup the other dictionary files if you aren’t reading the dictionary from the ROM.
    * Even if no dictionary is present, you’ll have to set up this file anyway. Just mirror the CHR file but leave the entries blank. Then set “MirrorBlankDTEEntries” to contain “Y”, detailed below.
* `New.xml`
    * The script to insert.
* `Original.xml`
    * The dumped script.
* `Menu.xml`
    * For the listbox window in Cypro Ace.
* `Comment.xml`
    * For the comment window in Cypro Ace.
## Example `Main.cypro` file:

```
<?xml version="1.0" encoding="utf-8"?>
<Root>
	<Options>
		<Original>{0}\EarthBound (USA).sfc</Original>
		<Blanked>{0}\EarthBound (BLANKED).sfc</Blanked>
		<BlankOutTextDataAfterRead>N</BlankOutTextDataAfterRead>
		<BlankOutByte>0x62</BlankOutByte>
		<MirrorBlankDictionaryEntries>Y</MirrorBlankDictionaryEntries>
	</Options>
	<ScriptManager>
		<DisplayReplaceCollection>
			<e>
				<Find>\[Left Parenthesis\]</Find>
				<Replace>(</Replace>
			</e>
			<e>
				<Find>\[Right Parenthesis\]</Find>
				<Replace>)</Replace>
			</e>
			<e>
				<Find>\[.*?\]</Find>
				<Replace>#</Replace>
			</e>
			<e>
				<Find>\(LINE\)</Find>
				<Replace>\r\n</Replace>
			</e>
			<e>
				<Find>\(NEXT\)</Find>
				<Replace>\r\n</Replace>
			</e>
			<e>
				<Find>\(name\)</Find>
				<Replace>NAME</Replace>
			</e>
			<e>
				<Find>\@</Find>
				<Replace>*</Replace>
			</e>
		</DisplayReplaceCollection>
	</ScriptManager>
	<Pointer>
		<e>
			<Name>Dictionary</Name>
			<Address CustomFormat="(B0),(B1),(B2),00">0x08CDED</Address>
			<PcDifference>-0xC00000</PcDifference>
			<AddressConversion>0</AddressConversion>
			<EntryNumber>0x300</EntryNumber>
			<DialogueReadType Delim="00">0</DialogueReadType>
			<HasSecondaryPointer>N</HasSecondaryPointer>
			<PrimaryPointerType>11</PrimaryPointerType>
			<PrimaryPointerLength>4</PrimaryPointerLength>
			<SecondaryPointerType>-1</SecondaryPointerType>
			<SecondaryPointerLength>0</SecondaryPointerLength>
			<RenderType DictionaryInteractionType="1">0</RenderType>
			<IsDictionary>Y</IsDictionary>
			<CHRFile>CHR</CHRFile>
			<DictionaryTemplateFile>Dictionary</DictionaryTemplateFile>
			<SquishyTextFile></SquishyTextFile>
			<PixelsPerLine>0</PixelsPerLine>
			<InserLineBreaks>N</InserLineBreaks>
			<AutoLineBreak>-1</AutoLineBreak>
			<StoreOutOfOrderToSaveSpace>Y</StoreOutOfOrderToSaveSpace>
		</e>
		<e>
			<Name>Dialogue</Name>
			<Address CustomFormat="(B0),(B1),(B2),XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX">0x0F899f</Address>
			<PcDifference>-0xC00000</PcDifference>
			<AddressConversion>0</AddressConversion>
			<EntryNumber>1406</EntryNumber>
			<DialogueReadType Delim="02|13,02">0</DialogueReadType>
			<HasSecondaryPointer>N</HasSecondaryPointer>
			<PrimaryPointerType>11</PrimaryPointerType>
			<PrimaryPointerLength>17</PrimaryPointerLength>
			<SecondaryPointerType>-1</SecondaryPointerType>
			<SecondaryPointerLength>0</SecondaryPointerLength>
			<RenderType DictionaryInteractionType="1">1</RenderType>
			<IsDictionary>N</IsDictionary>
			<CHRFile>CHR</CHRFile>
			<DictionaryTemplateFile>Dictionary</DictionaryTemplateFile>
			<SquishyTextFile></SquishyTextFile>
			<PixelsPerLine>0</PixelsPerLine>
			<InserLineBreaks>N</InserLineBreaks>
			<AutoLineBreak>-1</AutoLineBreak>
			<StoreOutOfOrderToSaveSpace>Y</StoreOutOfOrderToSaveSpace>
			<IgnoreNegativeAndZeroPointers>Y</IgnoreNegativeAndZeroPointers>
		</e>
		<e>
			<Name>Battle Text</Name>
			<Address CustomFormat="(B0),(B1),(B2),XX,XX,XX,XX,XX,XX,XX,XX,XX">0x157b78</Address>
			<PcDifference>-0xC00000</PcDifference>
			<AddressConversion>0</AddressConversion>
			<EntryNumber>317</EntryNumber>
			<DialogueReadType Delim="02|13,02">0</DialogueReadType>
			<HasSecondaryPointer>N</HasSecondaryPointer>
			<PrimaryPointerType>11</PrimaryPointerType>
			<PrimaryPointerLength>12</PrimaryPointerLength>
			<SecondaryPointerType>-1</SecondaryPointerType>
			<SecondaryPointerLength>0</SecondaryPointerLength>
			<RenderType DictionaryInteractionType="1">1</RenderType>
			<IsDictionary>N</IsDictionary>
			<CHRFile>CHR</CHRFile>
			<DictionaryTemplateFile>Dictionary</DictionaryTemplateFile>
			<SquishyTextFile></SquishyTextFile>
			<PixelsPerLine>0</PixelsPerLine>
			<InserLineBreaks>N</InserLineBreaks>
			<AutoLineBreak>-1</AutoLineBreak>
			<StoreOutOfOrderToSaveSpace>Y</StoreOutOfOrderToSaveSpace>
			<IgnoreNegativeAndZeroPointers>Y</IgnoreNegativeAndZeroPointers>
		</e>
	</Pointer>
	<Write>
		<ROMToCopy>{0}\EarthBound (trans).sfc</ROMToCopy>
		<ROMToWrite>{0}\EarthBound (NEW).sfc</ROMToWrite>
		
		<WriteableRangeCollection>
			<Range>
				<StartAddress>0x0</StartAddress>
				<Size>0x0</Size>
			</Range>				
		</WriteableRangeCollection>
		<WriteRegionCollection>
			<WriteRegion>
				<EntryOwners>
					<e>0</e>
					<e>1</e>
				</EntryOwners>
				<WriteableAddressRanges>
					<e>0</e>
					<e>1</e>			
				</WriteableAddressRanges>
			</WriteRegion>
		</WriteRegionCollection>
	</Write>
</Root>
```

## Example `Dictionary_template.tbl` file:

`[...]` indicates truncation for the sake of this readme. Please note this dictionary template file for Earthbound is incomplete.

```
1500=
[...]
17FF=
50=
[...]
AA=

//Control Codes
00=(LINE)
01=(NEWLINE)
02=(EOB)
13=(WAIT)
14=(PROMPT)
0300=(NEXT)
1302=(END)
04=(SET)<2>
05=(UNSET)<2>
06=(CONTROL CODE 06)<4>
07=(ISSET)<2>
08=(CALL)<4>
0A=(GOTO)<4>
10=(PAUSE)<1>
12=(CLEARLINE)

//Windows
1801=(window_open)<1>
1800=(window_closetop)
1803=(window_switch)<1>
1804=(window_closeall)
1806=(window_clear)
180A=(open_wallet)
1C04=(open_hp)

//Memory-related commands

1F52=(input)<3>
0E=(counter)<1>
0F=(inc)
1B04=(swap)
0D01=(ctoarg)
0D00=(rtoarg)
0B=(result_is)<1>
0C=(result_not)<1>
1B00=(store_registers)
1B01=(load_registers)
1B03=(CONTROL CODE 1B03)<4>


// Text control
1805=(text_pos)<2>
1C00=(text_color)<1>
1F04=(text_blips)<1>
1F30=(font_normal)
1F31=(font_saturn)
1C01=(stat)<1>
1C02=(name)<1>
1C05=(itemname)<1>
1C06=(teleportname)<1>
1C0801=(smash)
1C0802=(youwon)
1C0A=(number)<4>
1C0B=(money)<4>
1C0D=(user)
1C0E=(target)
1C0F=(delta)
1C12=(psiname)<1>

//Goods and money
1D00=(give)<1>
1D01=(take)<1>
1D02=(CONTROL CODE 1d02)<1>
1D03=(fullness)<1>
1D04=()<1>
1D05=(hasitem)<2>
1D06=(deposit)<4>
1D07=(withdraw)<4>
1D08=(givemoney)<2>
1D09=(takemoney)<2>
1D14=(hasmoney)<4>
1f81=(usable)<2>

//Stats
1905=(inflict)<3>
1E00=(heal_percent)<2>
1E01=(hurt_percent)<2>
1E02=(heal)<2>
1E03=(hurt)<2>
1E04=(recoverpp_percent)<2>
1E05=(consumepp_percent)<2>
1E06=(recoverpp)<2>
1E07=(consumepp)<2>
1E08=(change_level)<2>
1E09=(boost_exp)<4>
1E0A=(boost_iq)<2>
1E0B=(boost_guts)<2>
1E0C=(boost_speed)<2>
1E0D=(boost_vitality)<2>
1E0E=(boost_luck)<2>

//Sound and music
1F02=sound<1>
1F0000=(music)<1>
1F0102=(music_stop)
1F03=(music_resume)
1F05=(music_switching_off)
1F06=(music_switching_on)
1F07=(music_effect)<1>
1F=(CONTROL CODE 1F)<2>

//Gameplay control

1F21=(warp)<1>
1F20=(teleport)<2>
1F68=(anchor_set)
1F69=(anchor_warp)
1F11=(party_add)<1>
1F12=(party_remove)<1>
1F41=(event)<1>

1F71=(learnpsi)<2>
1FB0=(save)
1FE5=(lock_movement)<2>
1F66=(hotspot_on)<6>
1F67=(hotspot_off)

//Visual effects
1F13=(char_direction)<2>
1F16=(sprite_direction)<3>
1FEC=(show_char)<2>
1F1C=(show_char_float)<2>
1F1A=(show_sprite_float)<3>
1FEB=(hide_char)<2>
1F1D=(hide_char_float)<1>
1F1E=(hide_sprite)<1>
1F1B=(hide_sprite_float)<2>
```